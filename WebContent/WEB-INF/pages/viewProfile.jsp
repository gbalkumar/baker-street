<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<jsp:include page="header.jsp"/>

<title>Baker Street</title>
</head>
<body>
	
	<jsp:include page="navigationBar.jsp"/>
	  <c:if test = "${not empty profileNotComplete}">
	  	   ${profileNotComplete}
  	</c:if>
	
	<h1>Your Profile</h1>
	<h2>Name:<p>${user.userName}</p></h2>
	<h2>Address:<p>${user.userAddress}</p></h2>
	<h2>Phone Number:<p>${user.phoneNumber}</p></h2>
	<h2>Website URL:<p>${user.webURL}</p></h2>
	<a href="/user/profile/edit" role="button">Edit</a>
</body>
</html>