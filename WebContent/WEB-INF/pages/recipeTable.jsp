 <div class="container" style="margin-top:10px">
	
	<table class="table" id="dataTable">
	<thead>		
	  <tr id="tableheader"> 
	  		<!-- <td><b>Recipe Id</b></td> -->
	  		<td><b>Recipe Name</b></td>
	  		<td><b>Style</b></td>
	  		<td><b>Submitter email</b></td>
	  		<td><b>Views</b></td>
	  		<td><b>Average Rating</b></td>
	  		<c:if test="${isAdmin == true}">
	  			<td><b>Approve Status</b></td>	  			
	  		</c:if>
	  		

	  </tr>
	  </thead>
	  <tbody id="tablebody" >
	  
	  <c:forEach items="${recipeList}" var="recipe">
	  	<tr>
	  		<%-- <td>  <a href="/recipe/${recipe.recipeId}/${recipe.recipeName}">${recipe.recipeId}</a></td> --%>
	  		<td> <a href="/recipe/${recipe.recipeId}/${recipe.recipeName}">${recipe.recipeName}</a> </td>
	  		<td> ${recipe.styleName}</td>
	  		<td> ${recipe.submitterEmail} </td>
	  		<td> ${recipe.viewCount} </td>
	  		<td> ${recipe.averageRating}</td>
	  		<c:if test="${isAdmin == true}">
	  			<td> ${recipe.approved}</td>	
	  		</c:if>
	  		
	  	</tr>
	  
	  </c:forEach>
	  </tbody>		  
	</table>
</div>
