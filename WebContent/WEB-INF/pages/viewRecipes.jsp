<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="header.jsp"/>
	
	<script type="text/javascript" src="<c:url value="https://code.jquery.com/jquery-2.1.4.js"/>" > </script> 
	<script type="text/javascript" src="<c:url value="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"/>" > </script> 
	<script type="text/javascript"  src="<c:url value="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"/>"></script>
	
	<script>
		$(document).ready(function(){
				$('#dataTable').dataTable();
		});
	</script>

</head>
<body>

	<jsp:include page="navigationBar.jsp"/>
	
	
	 <c:if test="${isAdmin == true}">
		<h1>Displaying ALL <c:if test="${not empty adminUnapproved}"> unapproved </c:if> recipes</h1>
	</c:if>
	
	<c:if test="${isAdmin == false}">
		<h1>Displaying all approved <c:if test="${not empty userFavourite}"> user favourite </c:if> recipes</h1>
	</c:if> 

	
	<%-- <jsp:include page="recipeTable.jsp"></jsp:include> --%>
	<%@include file="recipeTable.jsp" %>
</body>
</html>