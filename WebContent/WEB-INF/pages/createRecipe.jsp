<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<jsp:include page="header.jsp"/>

    <title>Creating recipe</title>
    
    <%-- <script src="<c:url value="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" />"></script> --%>
	<%-- <link rel="stylesheet" href = "<c:url value="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.css"/>">
    <script src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.js" />"></script> --%>
    
    
    <script src="<c:url value="/resources/js/jquery-1.11.2.js"/>"></script> 
    <script src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css"/>">
    </head>
    <body>
    		<jsp:include page="navigationBar.jsp"/>
    	
        <h3>Welcome, Enter The Recipe Details</h3>

        <c:if test="${not empty message}">
			<strong>${message}</strong> 				
  		</c:if>
        <form:form method="POST" action="/recipe/createRecipe" modelAttribute="recipe" enctype="multipart/form-data" commandName="recipe">
             <table id="mainTable">
                <tr>
                    <td><form:label path="recipeName">Recipe Name</form:label></td>
                    <td><form:input path="recipeName"/></td>
                   	<td><form:errors path="recipeName" /></td>
                    
                </tr>
                <tr>
                    <td><form:label path="recipeHeading">Recipe Heading</form:label></td>
                    <td><form:input path="recipeHeading"/></td>
                    <td><form:errors path="recipeHeading" /></td>
                </tr>
                <tr>
                    <td><form:label path="styleName">Style Name</form:label></td>
                    <td><form:input path="styleName"/></td>
                    <td><form:errors path="styleName" /></td>
                </tr>
                <tr>
                    <td><form:label path="preparationTime">Preparation Time</form:label></td>
                    <td><form:input path="preparationTime"/></td>
                    <td><form:errors path="preparationTime" /></td>
                </tr>
                <tr>
                    <td><form:label path="cookTime">Cook Time</form:label></td>
                    <td><form:input path="cookTime"/></td>
                    <td><form:errors path="cookTime" /></td>
                </tr>
                <tr>
                    <td><form:label path="readyInTime">Ready In time</form:label></td>
                    <td><form:input path="readyInTime"/></td>
                </tr>
<%--                 <tr>
                    <td><form:label path="submitterId">Submitter ID</form:label></td>
                    <td><form:input path="submitterId"/></td>
                </tr> --%>
                
                <tr>
                	<td><h4>Instructions</h4></td>
                </tr>
                
                <c:set var="totalInstructionCount" value="2"/>
                <c:if test="${recipe.instructionList.size() > 2 }">
                	<c:set var="totalInstructionCount" value="${recipe.instructionList.size() - 1}"/>
                </c:if>
                
                <c:forEach var="counter" begin="0" end="${totalInstructionCount}">
               		
                	<tr id="instruction-table-row-${counter}">
                		<td><form:label path="instructionList[${counter}].instructionStep">Instruction ${counter}: </form:label></td>
                		<%-- <td><form:label path="instructionList[${counter}].instruc"> Instruction ${counter }</form:label></td> --%>
                		<td><form:input path="instructionList[${counter}].instruction" /></td>
                		<td><form:input path="instructionList[${counter}].instructionStep" cssStyle="display:none;" value="${counter}"></form:input></td>
                		
                	</tr>
                

               	 </c:forEach>
               	 				                   
                <tr>
                	<td><button id="add-button">+</button></td>
                	<!-- <td><a href="" id="add-button">+</a></td> -->
                </tr>
                
                <c:set var="totalIngredientCount" value="2"/>
                <c:if test="${recipe.ingredientList.size() > 2 }">
                	<c:set var="totalIngredientCount" value="${recipe.ingredientList.size() - 1}"/>
                </c:if>
                <tr>
                	<td></td>
               		<td><strong>Quantity</strong></td>
               		<td><strong>Metric</strong></td>
               		<td><strong>Ingredient Name</strong></td>
               	</tr>
                 <c:forEach var="ingredientCounter" begin="0" end="${totalIngredientCount}">

                	<tr id="ingredient-table-row-${ingredientCounter}">
                		<td>Ingredient ${ingredientCounter}:</td>
                		<td><form:input path="ingredientList[${ingredientCounter}].quantity"/></td>
                		<td><form:input class="metricNameAutoClass" path="ingredientList[${ingredientCounter}].metricName"/></td>
                		<td><form:input class="ingredientNameAutoClass" path="ingredientList[${ingredientCounter}].ingredientName"/></td>
                		
                	</tr>
                
                </c:forEach>   
      			<tr>
                	<td><button id="add-ingredient-button">+</button></td>
                </tr> 
                
                
                <tr>
                	<td><h4>Tips</h4></td>
                </tr>
                
                <c:set var="totalTipCount" value="0"/>
                <c:if test="${recipe.tipList.size() > 0 }">
                	<c:set var="totalTipCount" value="${recipe.tipList.size() - 1}"/>
                </c:if>
                
                <c:forEach var="counter" begin="0" end="${totalTipCount}">
               		
                	<tr id="tip-table-row-${counter}">
                		<td><label>Tip ${counter}: </label></td>
                		
                		<td><form:input path="tipList[${counter}].tip" /></td>
                		
                	</tr>
                

               	 </c:forEach>
               	 				                   
                <tr>
                	<td><button id="add-tip-button">+</button></td>
                	<!-- <td><a href="" id="add-button">+</a></td> -->
                </tr>
                
                <tr>
                	<td><form:label path="images">Images</form:label></td>
                	<td><form:input type="file" path="images" multiple="multiple"></form:input></td>
                </tr> 
                <!-- <tr>
                	<td><label for="images">Images</label></td>
                	<td><input type="file" name="images" multiple="multiple"></input></td>
                </tr>   -->                                                     
                <tr>
        			 <td colspan="2"><input type="submit" value="SubmitRecipe" /></td>    
                </tr>
                
                
            </table>
        </form:form>
        
        <a href="/" class="btn btn-primary" role="button">Cancel</a>
        
    </body>
    <script type="text/javascript">
    	var current_instruction_counter = ${totalInstructionCount};
    	var current_tip_counter = ${totalTipCount};
    	var ingredient_counter = ${totalIngredientCount};
		console.log("Instruciton counter: " + current_instruction_counter);

    </script>
    
    <script type="text/javascript"> var ingredientAutoCompleteListJavascript = ${jsonIngredientsList}</script>
    <script type ="text/javascript"> var metricAutoCompleteListJavascript = ${jsonMetricsList} </script>
        
    <script type="text/javascript" src="<c:url value="/resources/js/createRecipe.js" />"></script>
</html>