 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>

<nav class="navbar navbar-inverse navbar-fixed">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/">
      Baker-Street
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/">Home</a></li>
      
        <li><a href="/recipes">View Recipes</a></li>
        <li><a href="#">Search Recipe</a></li>
         <c:if test="${not empty isAuthenticatedUser}">
        	     
  			  <li><a href="/recipe">Submit Recipe</a></li>
     	</c:if>  
        
      </ul>
      <ul class="nav navbar-nav navbar-right">
      	<c:choose>
                <c:when test="${not empty isAuthenticatedUser}">
                	  <li class="dropdown">
				          <a class="dropdown-toggle" data-toggle="dropdown" href="#">MyAccount<span class="caret"></span></a>
				          <ul class="dropdown-menu">
					            <li><a href="/user/profile">Update Profile</a></li>

					            <li><a href="/user/favourite">View Favourite Recipes</a></li>
					            
					             <c:choose>

						            <c:when test="${isAdmin == true}">
						            	<li><a href="/user/approve">Approve Recipe</a></li>	
						            </c:when>
						            <c:otherwise>
						            	<li><a href="/message">Send Message To Admin</a></li>
						            </c:otherwise>    
						        </c:choose> 
 				          </ul>
			        	</li>
        	
						<li><a href="/user/logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a></li>
				     
                </c:when>
                <c:otherwise>
 						
       					 <li><a href="/user/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        		
 
                </c:otherwise>
            </c:choose>
      	
      	
      </ul>
    </div>
  </div>
</nav>
  
