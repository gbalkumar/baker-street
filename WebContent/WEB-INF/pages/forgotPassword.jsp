<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<jsp:include page="header.jsp"/>

<title>Forgot Password</title>
</head>
<body>
		<jsp:include page="navigationBar.jsp"/>
			
			 <c:if test="${not empty forgotPasswordMessage}">
				<strong>${forgotPasswordMessage}</strong> 
				 <c:if test="${not empty email}">
				 		<strong>${email}</strong> 
				 	
				 </c:if> 
  			</c:if> 
  			
			 <h1>Please enter your email to change your password</h1>
	 		<form:form  method="POST" action="/user/forgot-password" modelAttribute="user">
	 			<table>
		            <tr>
		               <td><form:label path = "userEmail" >Email</form:label></td>
		               <td><form:input path = "userEmail" placeholder="Enter Email"/></td>
		               <td><form:errors path="userEmail" /></td>
		               
		            </tr>
		            <tr>
		               <td colspan = "2">
		                  <input type = "submit" value = "Submit"/>
		               </td>
		            </tr>
         		</table> 
	 				
	 		
	 		</form:form>
	 	
	 	
</body>
</html>