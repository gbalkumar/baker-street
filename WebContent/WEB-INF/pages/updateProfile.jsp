<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<jsp:include page="header.jsp"/>

<title>Update Profile</title>
</head>
<body>
	<jsp:include page="navigationBar.jsp"/>
	
	<c:if test="${not empty profileNotComplete}">
		<strong>${profileNotComplete}</strong>
	
	</c:if>
	<h1>update profile</h1>
		<form:form  method="POST" action="/user/profile/edit" modelAttribute="user" commandName="user">
	   <table>
            <tr>
               <td><form:label path = "userName" >Name</form:label></td>
               <td><form:input path = "userName" placeholder="Enter Name"/></td>
             	
            </tr>
            <tr>
             <td><form:label path = "userAddress" >Address</form:label></td>
             <td><form:textarea path="userAddress" rows="5" cols="30" /></td>
            </tr>
            <tr>
               <td><form:label path = "phoneNumber">Phone Number</form:label></td>
               <td><form:input  path = "phoneNumber" placeholder="Enter Phone Number" /></td>
               <td><form:errors path="phoneNumber" /></td>
               
               
            </tr>
             <tr>
               <td><form:label path = "webURL">WebSite</form:label></td>
               <td><form:input  path = "webURL" placeholder="Enter Website URL if you have any" /></td>
               
            </tr>
        	
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Submit"/>
               </td>
            </tr>
            
         </table>    
	
	</form:form>
	<a href="/user/profile" class="btn btn-primary" role="button">Cancel</a>
	
	
</body>
</html>