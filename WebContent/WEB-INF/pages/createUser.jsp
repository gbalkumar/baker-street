<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<jsp:include page="header.jsp"/>

<title>Sign Up</title>
<script type="text/javascript"  src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script  type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
	
		<jsp:include page="navigationBar.jsp"/>
	
	<c:if test="${not empty createUserMessage}">
				<strong>${createUserMessage}</strong> 
  	</c:if>
	

	<form:form method="POST" action="/user/register" modelAttribute="user" commandName="user">

	<%-- <form:form method="POST" action="/user/createUser" modelAttribute="user" commandName="user"> --%>

	   <table>
            <tr>
               <td><form:label path = "userEmail">Email</form:label></td>
               <td><form:input path = "userEmail" /></td>
               <td><form:errors path="userEmail" /></td>
               
            </tr>
            	 
            <tr>
               	<td><form:label path = "userPassword" >Password</form:label></td>
               	<td><form:input id="password" path = "userPassword" placeholder="Enter Password" type="password"/></td>
            	<td><form:errors path="userPassword" /></td>
            	<td> 
            		<c:if test="${empty errorStatus}">
            			<p> password must be between 6 to 15 characters with atleast one lower-case and one numeric character please</p>
            		</c:if>
            	</td>
            </tr>
             <tr>
               <td><label >ConfirmPassword</label></td>
               <td><input id="confirmPassword" type="password" placeholder="Enter confirm password"/></td>
               <td> <span id='message'></span></td>
            </tr>
      
            <tr>
               <td colspan = "2">
		                  <button id="button" type = "submit" disabled>Submit</button>
               </td>
            </tr>
         </table> 
	
	</form:form>
</body>

    <script src="<c:url value="/resources/js/updatePassword.js"/>"></script> 


</html>