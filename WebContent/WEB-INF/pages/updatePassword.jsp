<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<jsp:include page="header.jsp"/>

<title>Update Password</title>

</head>
<body>
	
	<h1>password update</h1>
	
	
	<form:form  method="POST" action="/change-password" modelAttribute="user">
	 			<table>
	 				  <tr>
            		   
            		   	<td><form:input  type="hidden" path = "userEmail"  value="${mailId}" readonly="true"/></td>
             		  	
            		</tr> 
	 				 
		            <tr>
		               <td><form:label path = "userPassword" >Password</form:label></td>
		               <td><form:input id="password" path = "userPassword" placeholder="Enter Password" type="password"/></td>
		            	  <td><form:errors path="userPassword" /></td>
		            </tr>
		             <tr>
		               <td><label >ConfirmPassword</label></td>
		               <td><input id="confirmPassword" type="password"/></td>
		               <td> <span id='message'></span></td>
		            </tr>
		            <tr>
		               <td colspan = "2">
		                  <button id="button" type = "submit" disabled>Submit</button>
		               </td>
		            </tr>
         		</table> 
	 				<a href="user" type="submit" >login</a>
	 		
	 		</form:form>
	
	
</body>
<script type="text/javascript"  src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script language="JavaScript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<c:url value="/resources/js/updatePassword.js"/>"></script> 

	
</html>