  
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<jsp:include page="header.jsp"/>

<title>Login</title>

	
</head>
<body>
		<jsp:include page="navigationBar.jsp"/>
		
		 <c:if test="${not empty loginMessage}">
				<strong>${loginMessage}</strong> 
  		</c:if> 
  	 	
		<%--  <c:if test="${not empty loginPageMessage}">
				<strong>${loginPageMessage}</strong> 
  		</c:if>  --%>
  	 	
	<form:form  method="POST" action="/user/login" modelAttribute="user" commandName="user">

	   <table>
            <tr>
               <td><form:label path = "userEmail" >Email</form:label></td>
               <td><form:input path = "userEmail" placeholder="Enter Email"/></td>
               <td><form:errors path="userEmail" /></td>
            </tr>
            <tr>
               <td><form:label path = "userPassword">Password</form:label></td>
               <td><form:input type="password" path = "userPassword" placeholder="Enter Password" /></td>
               <td><form:errors path="userPassword" /></td>
            </tr>
        	
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Submit"/>
               </td>
            </tr>
         </table> 
	<a href="/user/register">Register</a>
		<a href="/user/forgot-password">Forgot Pasword</a>
	
	</form:form>
	
</body>
</html>