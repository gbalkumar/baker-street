<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="header.jsp"/>

	<title>Displaying recipe</title>
	<link rel="stylesheet" href="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css"/>">
	<script src="<c:url value="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" />"></script>
	
	<script src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"/>"></script>
	
</head>
<body>
    <jsp:include page="navigationBar.jsp"/>

	<%-- <c:if test="${not empty message}">
		<strong>${message}</strong> 				
  	</c:if --%>
  	
  	 <br/><br/>
     <c:if test = "${recipe.approved == false}">
	  	 <form method="POST" action="/recipe/${recipeId}/approveRecipe">
	  			<input type = "submit" value = "APPROVE RECIPE"/>
	  	 </form>
  	</c:if>
  	 
  	<c:if test = "${recipe.approved == true && isAuthenticatedUser == true}"> 
  		<c:choose>
	  		<c:when test = "${recipe.alreadyFavouriteForUser == false }">
			    <form method="POST" action="/recipe/${recipeId}/addToFavourite">
			 		<input type = "submit" value = "ADD TO FAVOURITES"/>
			    </form>
		    </c:when>
		    <c:when test = "${recipe.alreadyFavouriteForUser == true }">
			    <form method="POST" action="/recipe/${recipeId}/removeFromFavourite">
			 		<input type = "submit" value = "REMOVE FROM FAVOURITES"/>
			    </form>
		    </c:when>
	    </c:choose>
    </c:if> 
    
	<%-- <h1 style="color:green"> Created Recipe with recipe ID: ${recipeId}</h1> --%>
	<h2>Recipe Name is: ${recipe.recipeName}</h2>
	<h2>Recipe Heading is: ${recipe.recipeHeading}</h2>
	<h2>Recipe Preparation Time is: ${recipe.preparationTime}</h2>
	<h2>Recipe Cook Time is: ${recipe.cookTime}</h2>
	<h2>Recipe ReadyIn Time is: ${recipe.readyInTime}</h2>
	<h2>Recipe Style Name is: ${recipe.styleName}</h2>
	<h2>Recipe Submitted by: ${recipe.submitterId}</h2>

	<h2>Instructions</h2>
	<c:forEach items="${recipe.instructionList}" var="instruction">
        <p>
            <h4>Instruction Step: ${instruction.instructionStep}</h4>
            <h4>Instruction Line: ${instruction.instruction}</h4>  
        <p>
    </c:forEach>
    
    <h2>Ingredient</h2>
	<c:forEach items="${recipe.ingredientList}" var="ingredient">
        <p>
        	
            <h4>${ingredient.quantity}</h4>
            <h4>${ingredient.metricName}</h4>  
            <h4>${ingredient.ingredientName}</h4> 
            
        </p>
    </c:forEach>
    
    <h2>Tips</h2>
	<c:forEach items="${recipe.tipList}" var="tip">
        <p>
            <h4>Tip Line: ${tip.tip}</h4>  
        <p>
    </c:forEach>
    
    
    <h2>Images</h2>
    
    <c:forEach var="imageCounter" begin="0" end="${recipe.images.size() - 1}">
    	<p>
    		<img height="120" width="120" src="/images/${recipe.recipeId}/${imageCounter}">
    	</p>
    </c:forEach>
    
   <c:if test = "${recipe.approved == true && isAuthenticatedUser == true}">   
	    <h2>Comment</h2>
	    <c:forEach items="${recipe.commentList}" var="comment">
	        <tr>
	
	            <td><br>${comment.commentUserEmail}</td>
	            <td>${comment.comment}</td> 
	            
	            <c:set var="Day" value="${comment.commentDay}"/>
	            <c:set var="Year" value="${comment.commentYear}"/>
	        	<c:set var="Hour" value="${comment.commentHour}"/>
	            
	            <c:choose>
	        		<c:when test="${Year > 0}">
	        		
	        			<td>${comment.commentYear} Years ago</td>
	        		
	        		</c:when>
	        		
	        		<c:when test="${Day > 0}">
	        			<td>${comment.commentDay} Days ago</td>
	        		
	        		</c:when>
	
	        		<c:otherwise>
	 					<c:if test = "${Hour > 0}">
	        		    	<td>${comment.commentHour} Hours ago </td>
	        		    </c:if>
	        			<td>${comment.commentMinute} Minutes ago</td>
	        		
	        		</c:otherwise>
	        	
	        	
	        	</c:choose>
	            
	            
	        </tr>
	    </c:forEach>
	   
	    <form:form method="POST" action="/recipe/${recipeId}/addComment" modelAttribute="comment">
	    
	    	<table>
	
	                <tr>
	                    <td><form:label path="comment">Comment</form:label></td>
	                    <td><form:input path="comment"/></td>
	                    <td><form:errors path="comment"/></td>
	                </tr>
	                <tr>
	                   <td colspan="2"><input type="submit" value="ADD Comment" /></td>    
	                
	                </tr>
			</table>
	    
	    
	    </form:form>
	    
		
	
	    <form action="/recipe/${recipeId}/rating" method="POST">
	    	<input id="rating" type="text" name="rating" value="0" style="display:none">
	    	<div id="starDiv"></div>
	    	<label id="ratingLabel"></label>
				<button type="submit">Rate</button>   	
	    </form>
	    
	</c:if> 
	
    
</body>
    <script type="text/javascript" src="<c:url value="/resources/js/displayRecipe.js"/>"></script>
</html>