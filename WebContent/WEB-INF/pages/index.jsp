
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   
<jsp:include page="header.jsp"/>
	
     <link rel="stylesheet" href="<c:url value="/resources/css/index.css"/>">

<title>Baker Street</title>
</head>
<body>
	
	<jsp:include page="navigationBar.jsp"/>
	

	<br/>
	<c:if test="${not empty message}">
		<strong>${message}</strong> 				
  	</c:if>
    <br/><br/>
    			
	<div class="container" >
		<h1>Top viewed 5 recipes</h1>
		<div class="row">
	<c:forEach items="${topRecipeList}" var="recipe">
		
		<div class="col-lg-3">
		<div class="thumbnail">
			<a href="/recipe/${recipe.recipeId}/${recipe.recipeName}">
			
				<%-- <h4>Recipe ID: ${recipe.recipeId} </h4> --%>
				<h4>Recipe Name: ${recipe.recipeName} </h4>
				<h4>Image: </h4>
		     	<img height="80" width="80" src="/images/${recipe.recipeId}/0">
				
			</a>
				
		</div>
			</div>	
	</c:forEach> 
	</div>

	</div>
	
	<div class="container" >
	<h1>Most popular 5 recipes</h1>
	<div class="row">
	<c:forEach items="${mostPopularRecipeList}" var="recipe">
		<div class="col-lg-3">
		
			<div class="thumbnail">
		
			
			<a href="/recipe/${recipe.recipeId}/${recipe.recipeName}">
			<h4>Recipe Name: ${recipe.recipeName} </h4>
			<h4>Image: </h4>
			
			<img height="80" width="80" src="/images/${recipe.recipeId}/0">
			<!-- <img src="images"> -->
			</a>
			</div>	
			
		</div>
		
	</c:forEach>
	</div>
	</div>
	 
	 <c:if test="${not empty userEmail}">
	 	
	 	<div class="container" >
	
		<h1>User(${userId}) with email(${userEmail}) favourite recipes</h1>
		<div class="row">
		<c:forEach items="${userFavouriteRecipeList}" var="recipe">
			 	
				<div class="col-lg-3">
				
			<div class="thumbnail">
		
			<a href="/recipe/${recipe.recipeId}/${recipe.recipeName}">
			
				<h4>Recipe Name: ${recipe.recipeName} </h4>
				<h4>Image: </h4>
				<img height="80" width="80" src="/images/${recipe.recipeId}/0">
				<!-- <img src="images"> -->
			</a>
			</div>
			
			</div>
					
		
			</c:forEach>
			</div>
		</div>
	</c:if>

	<c:if test="${isAdmin == false }">
		 	<div class="container" >
	
		<h1>Baker's favourite recipes</h1>
		<div class="row">
		<c:forEach items="${bakerFavouriteRecipeList}" var="recipe">
				
				<div class="col-lg-3">
			<div class="thumbnail">
		
			<a href="/recipe/${recipe.recipeId}/${recipe.recipeName}">

				<h4>Recipe Name: ${recipe.recipeName} </h4>

				<h4>Image: </h4>
				<img height="80" width="80" src="/images/${recipe.recipeId}/0">
				</a>
			</div>
			</div>			
		
		</c:forEach>
		</div>
	</div>
	</c:if>
	

	
</body>
 
</html>