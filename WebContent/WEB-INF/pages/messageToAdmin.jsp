 
<%@taglib uri = "http://www.springframework.org/tags/form" prefix ="form"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<jsp:include page="header.jsp"/>

<title>Message Admin</title>
</head>
<body>
		<jsp:include page="navigationBar.jsp"/>
			
			 <c:if test="${not empty status}">
				<strong>${status}</strong>
				<strong>${userEmail}</strong> 
  		</c:if> 
		<form:form  method="POST" action="/message/send" modelAttribute="message" commandName="message">
	   <table>
            <tr>
               <td><form:label path = "fromEmail" >Email</form:label></td>
            <td><form:input path = "fromEmail" value="${userEmail}"  readonly="true"/></td> 
               <td><form:errors path="fromEmail" /></td>
            </tr>
            <tr>
               <td><form:label path = "subject">Subject</form:label></td>
               <td><form:input  path = "subject" placeholder="Enter subject" /></td>
               <td><form:errors path="subject" /></td>
            </tr>
        	 <tr>
               <td><form:label path = "body">Message</form:label></td>
               <td><form:textarea   path = "body" placeholder="Enter The body " /></td>
               <td><form:errors path="body" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value ="Send Message"/>
               </td>
            </tr>
         </table> 
	
	
		</form:form>



</body>
</html>