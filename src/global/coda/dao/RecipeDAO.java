package global.coda.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import global.coda.mapper.RecipeMapper;
import global.coda.model.Comment;
import global.coda.model.Ingredient;
import global.coda.model.Instruction;
import global.coda.model.Recipe;
import global.coda.model.Tip;

public interface RecipeDAO {

	///public static DataSource getDataSource();
	//public static void setDataSource (DataSource dataSource);
	public RecipeMapper getRecipeMapper();
	public void setRecipeMapper(RecipeMapper recipeMapper);
	
	public List<String> IngredientAutoComplete ();
	public List <String> MetricAutoComplete();
	
	public boolean addToFavourite( int recipeId, int userId);
	public boolean removeFromFavourite( int recipeId, int userId);
	public List<Recipe> viewAllUnapprovedRecipes(int userId);
	public List<Recipe> viewAllApprovedRecipes();
	
	public Recipe viewOneRecipeDetails( int recipeId, int userId);
	public Recipe viewOneRecipeDetails( int recipeId); //to use when not logged in
	
	public List<Instruction> getInstructions (int recipeId);
	public List<Ingredient> getIngredients (int recipeId);
	public List<Comment> getComment (int recipeId);
	public List<Tip> getTips (int recipeId);


	public int addComment(Comment comment);
	public int createRecipe(Recipe recipe) throws SQLException;
	public boolean approveRecipe (int recipeId , int userId);
	public int insertSingleInstruction(Instruction instruction, int recipeId);
	public void insertSingleIngredient (Ingredient ingredient, int recipeId) throws SQLException;
	public int insertSingleTip(Tip tip, int recipeId);
	
	
	public int getMetricId (String metricName);
	public int createMetric ( String metricName);
	public int getIngredientId (String ingredientName);
	public int createIngredient( String ingredientName);
	public String getStyleName(int styleId);
	public int getStyleId ( String styleName);
	public int createStyleName (String styleName);
	
	public boolean isPresentStyleName ( String styleName );
	public boolean isPresentRecipeId (int recipeId);
	public boolean isPresentUserId (int userId);
	public boolean isUserAdmin ( int userId);
	public boolean isRecipeApproved(int recipeId);
	public boolean isPresentIngredientName ( String ingredientName );
	public boolean isPresentMetricName ( String metricName );
	public boolean isAlreadyFavourite (int recipeId, int userId);

	public List<Recipe> viewAllRecipes(int userId);

	public List<Recipe> viewTopRecipes();
	public List<Recipe> viewMostPopularRecipes();
	public List<Recipe> viewBakersFavouriteRecipes();
	public List<Recipe> viewUserFavouriteRecipes(int userId);
	public List<Recipe> viewUserFavouriteRecipesAllDetails(int userId);

	public List<MultipartFile> getImagesForRecipe(int recipeId);
	public int insertImage(MultipartFile image, int recipeId);
	public void insertRecipeStyleId (Recipe recipe, String styleName) throws SQLException;
	
	
	public boolean hasRecipes();

	public boolean hasUserAlreadyRated(int userId, int recipeId);
	public int createUserRecipeRating(int recipeId, int userId, double rating);
	
	public int incrementViewCountForRecipe(int recipeId);
	public double calculateAverageRatingForRecipe(int recipeId);
	public int updateAverageRatingForRecipe(int recipeId);

}
