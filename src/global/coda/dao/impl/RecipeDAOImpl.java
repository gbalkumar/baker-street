package global.coda.dao.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import global.coda.dao.RecipeDAO;
import global.coda.mapper.RecipeMapper;
import global.coda.model.Comment;
import global.coda.model.Ingredient;
import global.coda.model.Instruction;
import global.coda.model.Recipe;
import global.coda.model.Tip;
import global.coda.util.Constant;
import global.coda.util.SQLConstant;


public class RecipeDAOImpl implements RecipeDAO {
	
	private static final Logger recipeDAOLog = LogManager.getLogger(RecipeDAOImpl.class.getName());
	private RecipeMapper recipeMapper;
	private static  DataSource dataSource;
	
	
	public static DataSource getDataSource() {
		return dataSource;
	}

	public static void setDataSource (DataSource dataSource) {
		RecipeDAOImpl.dataSource = dataSource;
	}

	public RecipeMapper getRecipeMapper() {
		return recipeMapper;
	}

	public void setRecipeMapper(RecipeMapper recipeMapper) {
		this.recipeMapper = recipeMapper;
	}


	
	public List <String> MetricAutoComplete() {
		
		List <String> metricsPresentList = new ArrayList<String>();

		recipeDAOLog.trace("INSIDE RecipeDAO - MetricAutoComplete() function");

		try(Connection connection = dataSource.getConnection();) {

			String sql = SQLConstant.AUTOCOMPLETE_METRIC_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ResultSet resultSet =  preparedStatement.executeQuery();
			while (resultSet.next()) {
				metricsPresentList.add(resultSet.getString("metric_name"));
			}

			return metricsPresentList;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}
	
		
		
	}
	
	public List<String> IngredientAutoComplete () {
		
		List <String> ingredientsPresentList = new ArrayList<String>();
		recipeDAOLog.trace("INSIDE RecipeDAO - IngredientAutoComplete() function");
		try(Connection connection = dataSource.getConnection();) {

			String sql = SQLConstant.AUTOCOMPLETE_INGREDIENT_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ResultSet resultSet =  preparedStatement.executeQuery();
			while (resultSet.next()) {
				ingredientsPresentList.add(resultSet.getString("ingredient_name"));
			}

			return ingredientsPresentList;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}
		
		
	}
	
	public boolean addToFavourite( int recipeId, int userId) {
		
		boolean addToFavourtiteStatus = false;
		recipeDAOLog.trace("INSIDE RecipeDAO - addToFavourite() function");
		try(Connection connection = dataSource.getConnection();) {

			String sql = SQLConstant.ADD_RECIPE_TO_USER_FAVOURITE;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			preparedStatement.setInt(2, recipeId);

			int status = preparedStatement.executeUpdate();
			if(status > 0){
				addToFavourtiteStatus = true;		
			}
			return addToFavourtiteStatus;	
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
		
	}
	
	public boolean removeFromFavourite( int recipeId, int userId) {
		
		boolean removeFromFavouriteStatus = false;
		recipeDAOLog.trace("INSIDE RecipeDAO - removeFromFavourite() function");
		try(Connection connection = dataSource.getConnection();) {

			String sql = SQLConstant.REMOVE_RECIPE_FROM_USER_FAVOURITE;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			preparedStatement.setInt(2, recipeId);

			int status = preparedStatement.executeUpdate();
			if(status > 0){
				removeFromFavouriteStatus = true;		
			}
			return removeFromFavouriteStatus;	
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
		
	}
	
	/*
	 * Used by ADMIN to view all UNAPPROVED recipes
	 */
	public List<Recipe> viewAllUnapprovedRecipes(int userId){
		
		recipeDAOLog.trace("INSIDE RecipeDAO - viewAllUnapprovedRecipes() function");

		List<Recipe> recipeList = new ArrayList<Recipe>();
		
		try(Connection connection = dataSource.getConnection();) {
			//check if user has Admin rights
			if(isUserAdmin(userId) == true){
				String sqlQuery = SQLConstant.SELECT_ALL_UNAPPROVED_RECIPE;
				PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
				ResultSet resultSet = preparedStatement.executeQuery();
				recipeList = recipeMapper.viewAllRecipes(resultSet);
				
				return recipeList;
			}
			else{
				return null;
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	/*
	 * (non-Javadoc)
	 * @see global.coda.dao.RecipeDAO#viewAllApprovedRecipes()
	 * To view all APROVED recipes
	 */	
	@Override
	public List<Recipe> viewAllApprovedRecipes() {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - viewAllApprovedRecipes() function");

		List<Recipe> recipeList = new ArrayList<Recipe>();
		
		try(Connection connection = dataSource.getConnection();) {
			
			String sqlQuery = SQLConstant.SELECT_ALL_APPROVED_RECIPES;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = preparedStatement.executeQuery();
			recipeList = recipeMapper.viewAllRecipes(resultSet);
		
			return recipeList;
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
		
	public List<Recipe> viewAllRecipes(int userId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - viewAllRecipes() function");
		
		List<Recipe> recipeList = new ArrayList<Recipe>();
		
		try(Connection connection = dataSource.getConnection();) {
			if(isUserAdmin(userId) == true){

				String sqlQuery = SQLConstant.SELECT_ALL_RECIPES;
				PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
				ResultSet resultSet = preparedStatement.executeQuery();
				recipeList = recipeMapper.viewAllRecipes(resultSet);
			
				return recipeList;
			}
			else{
				recipeDAOLog.trace("INSIDE RecipeDAO - viewAllRecipes() function- USER NOT ADMIN");
				return null;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Recipe viewOneRecipeDetails( int recipeId, int userId ){
		
		recipeDAOLog.trace("INSIDE RecipeDAO - viewOneRecipeDetails() function");
		
		List<Instruction> instructionList = new ArrayList<Instruction>();
		List<Ingredient> ingredientList = new ArrayList<Ingredient>();
		List<Comment> commentList = new ArrayList<Comment>();
		List<Tip> tipList = new ArrayList<Tip>();
		
		try(Connection connection = dataSource.getConnection();) {
			
			String sqlQuery;
			if(isUserAdmin(userId) == true){
				sqlQuery = SQLConstant.SELECT_SINGLE_RECIPE_ADMIN;
			}
			else{
				sqlQuery = SQLConstant.SELECT_SINGLE_RECIPE;
			}
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			Recipe recipe  = new Recipe();
			recipe = recipeMapper.viewOneRecipeDetails(resultSet);
			recipe.setImages(getImagesForRecipe(recipe.getRecipeId()));
			int styleId = recipe.getStyleId();
			recipeDAOLog.trace("INSIDE RecipeDAO - viewOneRecipeDetails() function Style ID" +styleId);

			String styleName = getStyleName(styleId);
			recipeDAOLog.trace("INSIDE RecipeDAO - viewOneRecipeDetails() function Style ID" +styleId);

			recipe.setStyleName(styleName);
			if (recipe != null){
				
				//get instruction list
				instructionList = getInstructions(recipeId);
				
				for(int i=0;i<instructionList.size();i++){
					Instruction instruction = instructionList.get(i);
					instruction.setRecipeId(recipeId);
				} 
				recipe.setInstructionList(instructionList);
				
				//get Ingredient List
				ingredientList = getIngredients(recipeId);

				recipe.setIngredientList(ingredientList);
				
				//get CommentList
				commentList = getComment(recipeId);
				recipe.setCommentList(commentList);
				
				//get TipList
				tipList = getTips(recipeId);
				recipe.setTipList(tipList);
			}
			return recipe;
			
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
	
public Recipe viewOneRecipeDetails( int recipeId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - viewOneRecipeDetails() function");
		
		List<Instruction> instructionList = new ArrayList<Instruction>();
		List<Ingredient> ingredientList = new ArrayList<Ingredient>();
		List<Comment> commentList = new ArrayList<Comment>();

		
		try(Connection connection = dataSource.getConnection();) {
			
			String sqlQuery;
			sqlQuery = SQLConstant.SELECT_SINGLE_RECIPE;
		
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			Recipe recipe  = new Recipe();
			recipe = recipeMapper.viewOneRecipeDetails(resultSet);
			recipe.setImages(getImagesForRecipe(recipe.getRecipeId()));
			int styleId = recipe.getStyleId();
			recipeDAOLog.trace("INSIDE RecipeDAO - viewOneRecipeDetails() function Style ID" +styleId);

			String styleName = getStyleName(styleId);
			recipeDAOLog.trace("INSIDE RecipeDAO - viewOneRecipeDetails() function Style ID" +styleId);

			recipe.setStyleName(styleName);
			if (recipe != null){
				
				//get instruction list
				instructionList = getInstructions(recipeId);
				
				for(int i=0;i<instructionList.size();i++){
					Instruction instruction = instructionList.get(i);
					instruction.setRecipeId(recipeId);
				} 
				recipe.setInstructionList(instructionList);
				
				//get Ingredient List
				ingredientList = getIngredients(recipeId);

				recipe.setIngredientList(ingredientList);
				
				//get CommentList
				commentList = getComment(recipeId);
				recipe.setCommentList(commentList);
				
				
			}
			return recipe;
			
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	public List<Ingredient> getIngredients (int recipeId){
		
		recipeDAOLog.trace("INSIDE RecipeDAO - getIngredients() function");
		List<Ingredient> ingredientList = new ArrayList<Ingredient>();
		
		try(Connection connection = dataSource.getConnection();) {
			

			String sql = SQLConstant.SELECT_INGREDIENT_FOR_RECIPE_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			ingredientList = recipeMapper.getIngredient(resultSet);
			//System.out.println("IN DAO ingrList SIZE: "+ingredientList.size());
			return ingredientList;
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}

	
	}
	
	public List<Instruction> getInstructions (int recipeId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - getInstructions() function");

		
		List<Instruction> instructionList = new ArrayList<Instruction>();

		try(Connection connection = dataSource.getConnection();) {
			

			String sql = SQLConstant.SELECT_INSTRUCTION_FOR_RECIPE;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			instructionList = recipeMapper.getInstruction(resultSet);
			return instructionList;
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}
				
	}
	
	public List<Comment> getComment (int recipeId) {
	
		recipeDAOLog.trace("INSIDE RecipeDAO - getComment() function");
		List<Comment> commentList = new ArrayList<Comment>();

		try(Connection connection = dataSource.getConnection();) {
			String sql = SQLConstant.SELECT_COMMENT_FOR_RECIPE_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			commentList = recipeMapper.getComment(resultSet);
			
			for(int i=0; i< commentList.size(); i++) {
				
				Comment comment = commentList.get(i);
				getCommentTime(comment, recipeId);
				
				recipeDAOLog.trace("INSIDE RecipeDAO - getComment() function COmment ID:"+comment.getCommentId());
				recipeDAOLog.trace("INSIDE RecipeDAO - getComment() function COmment Year:"+comment.getCommentYear());
				recipeDAOLog.trace("INSIDE RecipeDAO - getComment() function COmment Day:"+comment.getCommentDay());
				recipeDAOLog.trace("INSIDE RecipeDAO - getComment() function COmment Hour:"+comment.getCommentHour());
				recipeDAOLog.trace("INSIDE RecipeDAO - getComment() function COmment Minute:"+comment.getCommentMinute());
				recipeDAOLog.trace("INSIDE RecipeDAO - getComment() function COmment Second:"+comment.getCommentSecond());
			}
			
			return commentList;
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}
		
	}
	public void getCommentTime (Comment comment, int recipeId){
		
		recipeDAOLog.trace("INSIDE RecipeDAO - getCommentTime() function");

		int commentId = comment.getCommentId();
		try(Connection connection = dataSource.getConnection();) {

			String sqlQuery = SQLConstant.GET_COMMENT_YEAR_DAYS_HOUR_MINS_SEC;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			preparedStatement.setInt(2, commentId);

			ResultSet resultSet = preparedStatement.executeQuery();
			recipeMapper.getCommentTime(resultSet, comment);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	/*
	 * (non-Javadoc)
	 * @see global.coda.dao.RecipeDAO#addComment(global.coda.model.Comment)
	 * adding comment for recipe 
	 * @param comment object
	 */	
	public int addComment(Comment comment) {
		
		if(isPresentRecipeId(comment.getCommentRecipeId()) && isPresentUserId(comment.getCommentUserId())) {
			try(Connection connection = dataSource.getConnection()) {
				String sqlQuery = SQLConstant.INSERT_COMMENT;
				PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setInt(1, comment.getCommentRecipeId());
	            preparedStatement.setInt(2, comment.getCommentUserId());
	            preparedStatement.setString(3, comment.getComment());
	            
	            int result = preparedStatement.executeUpdate();
	            ResultSet resultSetGeneratedKeys = preparedStatement.getGeneratedKeys();
	            ////System.out.println("Comment insertion return status: " +result);
				
				int generatedCommentId = -1;
				while(resultSetGeneratedKeys.next()) {
					generatedCommentId = resultSetGeneratedKeys.getInt(1);
				}
				
				return generatedCommentId;
			
			}
			
			catch(SQLException e) {
				e.printStackTrace();
				return -1;
			}
		}
		else{
			//needs to throw user exception
			recipeDAOLog.trace("INSIDE RecipeDAO - addComment() ERROR - NO SUCH USER_ID or RECIPE_ID");
			return -1;
		}
	}
	
	
	
	/*
	 * (non-Javadoc)
	 * @see global.coda.dao.RecipeDAO#createRecipe(global.coda.model.Recipe)
	 * Creating a recipe 
	 */
	public int createRecipe(Recipe recipe) throws SQLException {
	
		recipeDAOLog.trace("INSIDE RecipeDAO - createRecipe() function");

		int recipeId;
		int userId;
		
		try(Connection connection = dataSource.getConnection();){
			
			recipeId = 0;
			userId = recipe.getSubmitterId();
			if(isPresentUserId(userId)) {
			
				String sqlQuery;
				PreparedStatement preparedStatement;
				
				if(isUserAdmin(userId)){
					
					recipeDAOLog.trace("INSIDE RecipeDAO - createRecipe() function - USER IS ADMIN");

					sqlQuery = SQLConstant.ADMIN_INSERT_RECIPE;
					preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
					preparedStatement.setInt(1, recipe.getSubmitterId());
					preparedStatement.setString(2, recipe.getRecipeName());
					preparedStatement.setString(3, recipe.getRecipeHeading());
					preparedStatement.setInt(4, recipe.getPreparationTime());
					preparedStatement.setInt(5, recipe.getCookTime());
					preparedStatement.setInt(6, recipe.getReadyInTime());
				
					recipe.setApproved(true);
					preparedStatement.setBoolean(7,recipe.isApproved());
					
					preparedStatement.executeUpdate();

				}
				else{
					
					recipeDAOLog.trace("INSIDE RecipeDAO - createRecipe() function_ USER NOT ADMIN");

					sqlQuery = SQLConstant.INSERT_RECIPE;
					preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
				
					preparedStatement.setInt(1, recipe.getSubmitterId());
					preparedStatement.setString(2, recipe.getRecipeName());
					preparedStatement.setString(3, recipe.getRecipeHeading());
					preparedStatement.setInt(4, recipe.getPreparationTime());
					preparedStatement.setInt(5, recipe.getCookTime());
					preparedStatement.setInt(6, recipe.getReadyInTime());
					preparedStatement.executeUpdate();

				
				}
				
				ResultSet resultSetGeneratedKeys = preparedStatement.getGeneratedKeys();
				
				recipeId = -1;
				while(resultSetGeneratedKeys.next()) {
					recipeId = resultSetGeneratedKeys.getInt(1);
				}
				
				recipeDAOLog.trace("RecipeID created: " +recipeId);
				recipe.setRecipeId(recipeId);
				
				//insert Style ID in recipe Table
				insertRecipeStyleId (recipe, recipe.getStyleName());
				
				for(MultipartFile image: recipe.getImages()) {
					int insertedImageId = insertImage(image, recipeId);
					recipeDAOLog.trace("Image ID created: " + insertedImageId + " for recipe ID: " + recipeId + "having image name: " + image.getOriginalFilename());

				}
				for (Ingredient ingredient : recipe.getIngredientList()) {
					if(ingredient.getIngredientName().length() != 0){
						insertSingleIngredient(ingredient, recipeId);
						recipeDAOLog.trace("Ingredient inserted for recipe ID: " + recipeId);
					}
					else{
						recipeDAOLog.trace("Ingredient not created as it is null");

					}

				}

				for(Instruction instruction: recipe.getInstructionList()) {
					String instructionLine = instruction.getInstruction();
					if(instructionLine.length() == 0){
						recipeDAOLog.trace("Instruction ID  not created as it is null");
					}
					else{
						int insertedInstructionId = insertSingleInstruction(instruction, recipeId);
						recipeDAOLog.trace("Instruction ID created: " + insertedInstructionId+ " for recipe ID: " + recipeId);
					}
				}
				for(Tip tip: recipe.getTipList()) {
					String tipLine = tip.getTip();
					if(tipLine.length() == 0){
						recipeDAOLog.trace("Tip ID  not created as it is null");
					}
					else{
						int insertedTipId = insertSingleTip(tip, recipeId);
						recipeDAOLog.trace("Tip ID created: " + insertedTipId+ " for recipe ID: " + recipeId);
					}
				}

				return recipeId;
			}
			else {
				recipeDAOLog.trace("INSIDE RecipeDAO - createRecipe() ERROR - NO SUCH USER_ID");
				//throws User Exception
				return -1;
			}
		}

	}
	
	public void insertSingleIngredient (Ingredient ingredient, int recipeId) throws SQLException {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - insertSingleIngredient() function");

		
		String ingredientName = ingredient.getIngredientName();
		String metricName = ingredient.getMetricName();
		int ingredientId;
		int metricId;
		
		try (Connection connection = dataSource.getConnection(); ){
			
			if(isPresentIngredientName(ingredientName)) {			
				ingredientId = getIngredientId(ingredientName);	
			}
			else{			
				ingredientId = createIngredient(ingredientName);
			}
			
			if(isPresentMetricName(metricName)) {
				metricId = getMetricId(metricName);
				
			}
			else {			
				metricId = createMetric (metricName);
			}
				
			String sqlQuery = SQLConstant.INSERT_IN_RECIPE_INGREDIENT_REL;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			preparedStatement.setInt(2, ingredientId);
			preparedStatement.setInt(3, ingredient.getQuantity());
			preparedStatement.setInt(4, metricId);

			preparedStatement.executeUpdate();

		//return styleId;

		}

		
	}
	
	/*
	 * Getting metric id if metric name alrady exists.
	 */
	public int getMetricId (String metricName) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - getMetricId() function");
		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_METRIC_ID_WITH_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, metricName);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getInt(1);
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}
		
		return -1;
		
	}
	
	/*
	 * Creating new metric id if metric name does not exist.
	 */
	public int createMetric ( String metricName) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - createMetric() function");	
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.INSERT_METRIC_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, metricName);
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			int metricId = -1;
			while(resultSet.next()) {
				metricId = resultSet.getInt(1);
			}
			return metricId;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return -1;
		
	}
	
	/*
	 * Getting Ingredient Id if ingredient already exists
	 */
	public int getIngredientId (String ingredientName) {
		recipeDAOLog.trace("INSIDE RecipeDAO - getIngredientId() function");
		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_INGREDIENT_ID_WITH_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, ingredientName);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getInt(1);
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}
		
		return -1;

		
	}
	
	/*
	 * Creating Ingredient if Ingredient Name does not exist.
	 */
	public int createIngredient( String ingredientName) {
		recipeDAOLog.trace("INSIDE RecipeDAO - createIngredient() function");

		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.INSERT_INGREDIENT_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, ingredientName);
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			int ingredientId = -1;
			while(resultSet.next()) {
				ingredientId = resultSet.getInt(1);
			}
			return ingredientId;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return -1;
		
	}
	
	/*
	 * Insert Single Instruction into Instruction table
	 */
	public int insertSingleInstruction(Instruction instruction, int recipeId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - insertSingleInstruction()");
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.INSERT_SINGLE_INSTRUCTION;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			preparedStatement.setInt(2, instruction.getInstructionStep());
			preparedStatement.setString(3, instruction.getInstruction());

			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			int instructionId = -1;
			while(resultSet.next()) {
				instructionId = resultSet.getInt(1);
			}
			return instructionId;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return -1;

	}
	
	public int insertImage(MultipartFile image, int recipeId) {
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.INSERT_IMAGE;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			preparedStatement.setBytes(2, image.getBytes());
			preparedStatement.setString(3, image.getOriginalFilename());
			preparedStatement.setString(4, image.getContentType());
			int result = preparedStatement.executeUpdate();
			ResultSet resultSetGeneratedKeys = preparedStatement.getGeneratedKeys();
			
			int generatedImageId = -1;
			while(resultSetGeneratedKeys.next()) {
				generatedImageId = resultSetGeneratedKeys.getInt(1);
			}
			//recipeDAOLog.trace("Image ID created: " + generatedImageId + " for recipe ID: " + recipeId + "having image name: " + image.getOriginalFilename());
			return generatedImageId;
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public void insertRecipeStyleId (Recipe recipe, String styleName) throws SQLException {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - insertRecipeStyleId() function");
		int styleId =0;
		//int recipeId = recipe.getRecipeId();
		try (Connection connection = dataSource.getConnection(); ){
			
			if(isPresentStyleName(styleName)) {			
				styleId = getStyleId(styleName);	
				recipe.setStyleId(styleId);	

			}
			else{			
				styleId = createStyleName(styleName);
				recipe.setStyleId(styleId);		
			}
				
			String sqlQuery = SQLConstant.UPDATE_STYLE_ID_IN_RECIPE;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipe.getStyleId());
			preparedStatement.setInt(2, recipe.getRecipeId());
			preparedStatement.executeUpdate();

		//return styleId;

		}
	}
	
	public int createStyleName (String styleName){
		
		recipeDAOLog.trace("INSIDE RecipeDAO - createStyleName() function");
		recipeDAOLog.trace("Style: "+styleName);

		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.INSERT_STYLE_NAME;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, styleName);
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			int styleId = -1;
			while(resultSet.next()) {
				styleId = resultSet.getInt(1);
			}
			return styleId;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return -1;
		
	}
	/*
	 * Get style name from Style ID
	 */
	public String getStyleName(int styleId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - getStyleName() function");
			
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_STYLE_NAME_WITH_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, styleId);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getString(1);
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}
		
	
		return null;

		
	}
	
	/*
	 * Get style Id for style name
	 */
	public int getStyleId ( String styleName) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - getStyleId() function");
	
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_STYLE_ID_WITH_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, styleName);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getInt(1);
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}
		
		return -1;

		
	}
	
	/*
	 * Recipe Approval function
	 * @param Recipe ID and user ID
	 */
	public boolean approveRecipe (int recipeId , int userId){
		
		recipeDAOLog.trace("INSIDE RecipeDAO - approveRecipe()");
		try(Connection connection = dataSource.getConnection();) {
			
			if(isPresentRecipeId(recipeId) && isUserAdmin(userId)) {
				String sqlQuery = SQLConstant.APPROVE_RECIPE;
				PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setInt(1, recipeId);
				int status = preparedStatement.executeUpdate();
				
				if(status >0) {
					recipeDAOLog.trace("INSIDE RecipeDAO - approveRecipe() complete! recipe Id: "+recipeId +" approved");
					return true;
				}
				else {
					return false;
				}
			}
			else {
				recipeDAOLog.trace("INSIDE RecipeDAO - approveRecipe() ERROR - NO SUCH RECIPE_ID or NOT ADMIN");
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		}
	
	}
	

	
	
	
	/*
	 * Checking if user is admin - used in approveRecipe function
	 */
	public boolean isUserAdmin (int userId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - isUserAdmin()");
		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_ISADMIN;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getBoolean(1);
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
		
	}
	
	public boolean isRecipeApproved(int recipeId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - isRecipeApproved()");
		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_ISAPPROVED;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return resultSet.getBoolean(1);
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
		
	}
	
	public boolean isPresentStyleName ( String styleName ) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - isPresentStyleName() function");

		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_STYLE_ID_WITH_NAME;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, styleName);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		
		
	}
	
	public boolean isAlreadyFavourite (int recipeId, int userId){
		recipeDAOLog.trace("INSIDE RecipeDAO - isAlreadyFavourite() function");

		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_ISALREADY_FAVOURITE;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			preparedStatement.setInt(2, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean isPresentIngredientName ( String ingredientName ) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - isPresentIngredientName() function");

		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_INGREDIENT_ID_WITH_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, ingredientName);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		
		
	}
	
	public boolean isPresentMetricName ( String metricName ) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - isPresentMetricName() function");

		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_METRIC_ID_WITH_NAME;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, metricName);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		
		
	}
	
	

	public boolean isPresentRecipeId (int recipeId) {
		
		recipeDAOLog.trace("INSIDE RecipeDAO - isPresentRecipeId() function");
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_RECIPE_WITH_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;

		
	}

	public boolean isPresentUserId (int userId) {
		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.SELECT_USER_WITH_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.next()) {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		
		
	}
	
	
	public boolean hasRecipes() {
		try(Connection connection = dataSource.getConnection()) {
			String sqlQuery = SQLConstant.SELECT_RECIPE;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(!resultSet.next()) {
				return false;
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public List<Recipe> viewTopRecipes() {
		
		try(Connection connection = dataSource.getConnection()) {
			if(hasRecipes() == false) {
				//throw new NoObjectsException("recipes");
			}
			String sqlQuery = SQLConstant.SELECT_TOP_RECIPES;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, Constant.NUMBER_OF_RECIPES);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			return recipeMapper.viewTopRecipes(resultSet);
			
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public List<MultipartFile> getImagesForRecipe(int recipeId) {
		try(Connection connection = dataSource.getConnection()) {
			String sqlQuery = SQLConstant.SELECT_IMAGES_FOR_RECIPE_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			return recipeMapper.getImagesForRecipe(resultSet);
			
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	


	public boolean hasUserAlreadyRated(int userId, int recipeId) {
		try(Connection connection = dataSource.getConnection()) {
			String sqlQuery = SQLConstant.SELECT_USER_RATING_WITH_USER_ID_RECIPE_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			preparedStatement.setInt(2, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if(!resultSet.next()) {
				return false;
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public int createUserRecipeRating(int recipeId, int userId, double rating) {
		try(Connection connection = dataSource.getConnection()) {
			String sqlQuery;
			if(hasUserAlreadyRated(userId, recipeId) == false){
				sqlQuery = SQLConstant.INSERT_USER_RECIPE_RATING;
				recipeDAOLog.trace("User " + userId + " has NOT already rated for recipeId " + recipeId);

			}
			else {
				sqlQuery = SQLConstant.UPDATE_USER_RECIPE_RATING;
				recipeDAOLog.trace("User " + userId + " has already rated for recipeId " + recipeId);

			}
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setDouble(1, rating);
			preparedStatement.setInt(2, recipeId);
			preparedStatement.setInt(3,  userId);
			int updateStatus = preparedStatement.executeUpdate();
			
			int avgUpdateStatus = updateAverageRatingForRecipe(recipeId);
			return updateStatus;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public List<Recipe> viewMostPopularRecipes() {
		try(Connection connection = dataSource.getConnection()) {
			if(hasRecipes() == false) {
				//throw new NoObjectsException("recipes");
			}
			String sqlQuery = SQLConstant.SELECT_MOST_POPULAR_RECIPES;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, Constant.NUMBER_OF_RECIPES);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			return recipeMapper.viewTopRecipes(resultSet);
			
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Recipe> viewBakersFavouriteRecipes() {
		try(Connection connection = dataSource.getConnection()) {
			if(hasRecipes() == false) {
				//throw new NoObjectsException("recipes");
			}
			String sqlQuery = SQLConstant.SELECT_BAKER_FAVOURITE_RECIPES;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			//preparedStatement.setInt(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			return recipeMapper.viewTopRecipes(resultSet);
			
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Recipe> viewUserFavouriteRecipes(int userId) {
		try(Connection connection = dataSource.getConnection()) {
			if(hasRecipes() == false) {
				//throw new NoObjectsException("recipes");
			}
			String sqlQuery = SQLConstant.SELECT_USER_FAVOURITE_RECIPES;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			return recipeMapper.viewTopRecipes(resultSet);
			
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Recipe> viewUserFavouriteRecipesAllDetails(int userId) {
		try(Connection connection = dataSource.getConnection()) {
			if(hasRecipes() == false) {
				//throw new NoObjectsException("recipes");
			}
			String sqlQuery = SQLConstant.SELECT_ALL_APPROVED_USER_FAVOURITE_RECIPES;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			return recipeMapper.viewAllRecipes(resultSet);
			
			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int incrementViewCountForRecipe(int recipeId) {
		try(Connection connection = dataSource.getConnection()) {
			String sqlQuery = SQLConstant.INCREMENT_VIEW_COUNT_FOR_RECIPE_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			int updateStatus = preparedStatement.executeUpdate();
			return updateStatus;
		}

		catch(SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	

	public double calculateAverageRatingForRecipe(int recipeId) {
		try(Connection connection = dataSource.getConnection()) {
			String sqlQuery = SQLConstant.SELECT_AVERAGE_RATING_FOR_RECIPE_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			double avg = 0.0;
			while(resultSet.next()) {
				avg = resultSet.getDouble(1);
			}
			return avg;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return -1.0;
		}
	}
	
	public int updateAverageRatingForRecipe(int recipeId) {
		try(Connection connection = dataSource.getConnection()) {
			String sqlQuery = SQLConstant.UPDATE_AVERAGE_RATING_FOR_RECIPE_ID;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			double avg = calculateAverageRatingForRecipe(recipeId);
			preparedStatement.setDouble(1, avg);
			preparedStatement.setInt(2, recipeId);
			int updateStatus = preparedStatement.executeUpdate();
			return updateStatus;
		}

		catch(SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public List<Tip> getTips(int recipeId) {
		recipeDAOLog.trace("INSIDE RecipeDAO - getTips() function");

		
		List<Tip> tipList = new ArrayList<Tip>();

		try(Connection connection = dataSource.getConnection();) {
			

			String sql = SQLConstant.SELECT_TIP_FOR_RECIPE;
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			ResultSet resultSet = preparedStatement.executeQuery();
			tipList = recipeMapper.getTip(resultSet);
			return tipList;
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}
	}

	@Override
	public int insertSingleTip(Tip tip, int recipeId) {
		recipeDAOLog.trace("INSIDE RecipeDAO - insertSingleTip()");
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.INSERT_SINGLE_TIP;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, recipeId);
			preparedStatement.setString(2, tip.getTip());

			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			int tipId = -1;
			while(resultSet.next()) {
				tipId = resultSet.getInt(1);
			}
			return tipId;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

}
