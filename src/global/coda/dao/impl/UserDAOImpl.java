package global.coda.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import global.coda.dao.UserDAO;
import global.coda.model.User;
import global.coda.util.SQLConstant;

public class UserDAOImpl implements UserDAO {

	private static final Logger userDAOLog = LogManager.getLogger(UserDAOImpl.class.getName());
	private static  DataSource dataSource;
	
	public static DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	public int createUser(String mailId) {
		final String sqlQuery = SQLConstant.CREATE_USER;
		userDAOLog.trace("INSIDE userDAO - create User() function");

		try(Connection connection = dataSource.getConnection() ) {	
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1, mailId);
			
			
			int result = preparedStatement.executeUpdate();
			//System.out.println("Job insertion return status: " +result);

			ResultSet resultSetGeneratedKeys = preparedStatement.getGeneratedKeys();
			int userId = -2;
			while(resultSetGeneratedKeys.next()) {
				userId = resultSetGeneratedKeys.getInt(1);
			}
			userDAOLog.trace("userd created is "+userId);
			connection.close();
		return userId;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return -2;
		}
		
		
	}
	
	
	public int loginUser(User user) {
		
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.LOGIN_USER;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1,user.getUserEmail());
			preparedStatement.setString(2,user.getUserPassword());
		
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				connection.close();

				return rs.getInt(1);

			}
			userDAOLog.trace("INSIDE userDAO - login User() function");
			connection.close();

			return 0;
			
		}
		
		catch(SQLException e) {
			e.printStackTrace();
			return 0;
		}
		
	
	}
	
	public boolean updateProfile(int userId,User user) {
		
		final String sqlQuery = SQLConstant.UPDATE_PROFILE;
		userDAOLog.trace("INSIDE userDAO - update profile() function");

		try(Connection connection = dataSource.getConnection() ) {	
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, user.getUserName());

			preparedStatement.setString(2, user.getUserAddress());	

			preparedStatement.setString(3, user.getPhoneNumber());

			preparedStatement.setString(4, user.getWebURL());

			preparedStatement.setInt(5, userId);
			int result = preparedStatement.executeUpdate();
		
			if(result==0) {
				connection.close();

				return false;
			}
			
		
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;	
	}


	public boolean isUserExists(User user) {
		
		final String sqlQuery=SQLConstant.IS_USER_EXISTS;
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1, user.getUserEmail());

			ResultSet rs = preparedStatement.executeQuery();
			userDAOLog.trace("INSIDE userDAO - isUser Exists function"+user.getUserEmail());

			if(rs.next()) {
				connection.close();

				return true;
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
			return true;
		}
		
		return false;
	}


	
	public boolean isAlreadyRequested(User user) {
		final String sqlQuery=SQLConstant.IS_LINK_ALREADY_REQUESTED;
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1, user.getUserEmail());

			ResultSet rs = preparedStatement.executeQuery();
			
			if(rs.next()) {
				userDAOLog.trace("inside is Link already requested DAO rs.next() ");
				connection.close();

				return true;
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
			return true;
		}
		
		return false;
	}


	public boolean generateForgotPasswordLink(User user, String linkId) {
		final String sqlQuery=SQLConstant.GENERATE_FORGOT_PASSWORD_LINK;
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1, user.getUserEmail());
			preparedStatement.setString(2, linkId);

			int result= preparedStatement.executeUpdate();
			if(result>0) {
				connection.close();

				return true;
			}
		
			userDAOLog.trace("forgot password ink Id generated successfully created is "+linkId);
			return false;
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}


	
	public String isTokenValid(String token) {
		final String sqlQuery=SQLConstant.IS_TOKEN_EXISTS;
		
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1,token);
			ResultSet rs = preparedStatement.executeQuery();
			
			if(rs.next()) {
				connection.close();

				return rs.getString(1);
			}
			
		return null;
			
		} catch(SQLException e) {
			e.printStackTrace();
			
		}
	return null;
	}
	
	public void deleteExpiredTokens() {
		
		final String sqlQuery=SQLConstant.DELETE_EXPIRED_TOKENS;
		
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	
			int result = preparedStatement.executeUpdate();
		
			connection.close();

		} catch(SQLException e) {
			e.printStackTrace();
			
		}
			
	}

	public boolean updatePassword(User user) {
		
		final String sqlQuery=SQLConstant.UPDATE_PASSWORD;

		try(Connection connection = dataSource.getConnection()) {
					
					PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
		
					preparedStatement.setString(1, user.getUserPassword());
		
					preparedStatement.setString(2, user.getUserEmail());
					int result = preparedStatement.executeUpdate();
					userDAOLog.trace("password updated result status "+result);

					if(result==0) {
						connection.close();

						return false;
					}
				
				}
				catch(SQLException e) {
					e.printStackTrace();
					return false;
				}
		
				return true;	
	}


	public void deletePasswordUpdatedTokens(User user) {
		
		final String sqlQuery=SQLConstant.DELETE_PASSWORD_UPDATED_TOKENS;
		
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getUserEmail());

			int result = preparedStatement.executeUpdate();
			connection.close();

			
		} catch(SQLException e) {
			e.printStackTrace();
			
		}
		
	}

	public boolean isAdmin(String userEmail) {
		try(Connection connection = dataSource.getConnection();) {
			String sqlQuery = SQLConstant.IS_ADMIN;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1,userEmail);
			
		
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				connection.close();

				return rs.getBoolean(1);

			}
			userDAOLog.trace("INSIDE isAdmin function in user controller");

			return false;
			
		}
		
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	
		
	}

	public User userProfile(String email) {
		
		try(Connection connection = dataSource.getConnection();) {
			User user= new User();
			String sqlQuery = SQLConstant.SELECT_USER_PROFILE;
			//PreparedStatement preparedStatement = InputUtil.getPreparedStatement(sqlQuery);
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1,email);

			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				user.setUserName(rs.getString(1));
				user.setUserAddress(rs.getString(2));
				user.setPhoneNumber(rs.getString(3));
				user.setWebURL(rs.getString(4));	
			}
			userDAOLog.trace("INSIDE isAdmin function in user controller");
			
			userDAOLog.trace("inside UserServiceImpl- user profile user Name\n: "+user.getUserName());
			userDAOLog.trace("inside UserServiceImpl- user profile user Address \n: "+user.getUserAddress());
			userDAOLog.trace("inside UserServiceImpl-user profile user PhoenNumber\n: "+user.getPhoneNumber());
			userDAOLog.trace("inside UserServiceImpl- user profile user WebURL \n: "+user.getWebURL());
			connection.close();

			return user;
			
		}
		
		catch(SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	public boolean isUserValidated(User user) {
		
		final String sqlQuery=SQLConstant.IS_USER_IN_VALIDATION_QUEUE;

		try(Connection connection = dataSource.getConnection()) {
					
					PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

					preparedStatement.setString(1, user.getUserEmail());
					
					userDAOLog.trace("password updated result status ");

					ResultSet rs = preparedStatement.executeQuery();
					while(rs.next()) {
						connection.close();

						return false;

					}
				
				}
				catch(SQLException e) {
					e.printStackTrace();
					return false;
				}
		
				return true;	
	
		}

	public void generateUserValidationKey(User user,String key) {
		
		final String sqlQuery=SQLConstant.GENERATE_USER_VALIDATION_KEY;
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1, user.getUserEmail());
			preparedStatement.setString(2,user.getUserPassword());
			preparedStatement.setString(3,key);

			int result= preparedStatement.executeUpdate();
			
			connection.close();

			userDAOLog.trace("forgot password ink Id generated successfully created is "+key);

		} catch(SQLException e) {
			e.printStackTrace();
			
		}
		
		
		
		
	}

	public String isUserTokenValid(String token) {
		final String sqlQuery=SQLConstant.IS_USER_TOKEN_EXISTS;
		
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1,token);
			ResultSet rs = preparedStatement.executeQuery();
			
			if(rs.next()) {
				connection.close();

				return rs.getString(1);
			}
			
		return null;
			
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	
	}

	public void deleteExpiredUserTokens() {
		
		
	}

	public void deleteVerifiedUserTokens(String mailId) {
final String sqlQuery=SQLConstant.DELETE_USER_VERIFIED_TOKENS;
		
		try(Connection connection = dataSource.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, mailId);

			int result = preparedStatement.executeUpdate();
			connection.close();

			
		} catch(SQLException e) {
			e.printStackTrace();
			
		}
		
		
	}

}
