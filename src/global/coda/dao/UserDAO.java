package global.coda.dao;



import global.coda.model.User;

public interface UserDAO {
	
	public int createUser(String mailId);
	public int loginUser(User user) ;
	public boolean updateProfile(int userId,User user);
	public boolean isUserExists(User user);
	public boolean isAlreadyRequested(User user);
	public boolean generateForgotPasswordLink(User user, String linkId);
	public String isTokenValid(String token);
	public void deleteExpiredTokens();
	public boolean updatePassword(User user);
	public void deletePasswordUpdatedTokens(User user);
	public boolean isAdmin(String userEmail);
	public User userProfile(String userEmail);
	public boolean isUserValidated(User user);
	public void generateUserValidationKey(User user,String key);
	public String isUserTokenValid(String token);
	public void deleteExpiredUserTokens();
	public void deleteVerifiedUserTokens(String mailId);
		
		
}
