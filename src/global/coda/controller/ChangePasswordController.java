package global.coda.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import global.coda.model.User;
import global.coda.service.UserService;

@Controller
@RequestMapping(value="/change-password")
public class ChangePasswordController {
	
	private static final Logger forgotPasswordControllerLog = LogManager.getLogger(ChangePasswordController.class.getName());
	
	//forgotPasswordControllerLog.trace("inside forgotpassword controller");
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public ModelAndView updatePassword(@RequestParam String token,HttpSession httpSession) {
		ModelMap map= new ModelMap();
		
		forgotPasswordControllerLog.trace(token);
		String mailId= userService.isTokenValid(token);
		forgotPasswordControllerLog.trace("mailId returned"+mailId);

		if(mailId!=null) {
			forgotPasswordControllerLog.trace("inside forgotpassword controller GET , token is valid");
			//map.put("mailId", mailId);
			//User user=new User();
			//user.setUserEmail(mailId);
			httpSession.setAttribute("mailId",mailId);
			map.put("user",new User());
			return new ModelAndView("updatePassword",map);	
		}
		
		return new ModelAndView("errorPage");			
	}
	
	
	
	@PostMapping
	public ModelAndView updatePassword(@Valid @ModelAttribute("user")User user,BindingResult result,HttpSession httpSession) {
		forgotPasswordControllerLog.trace("inside POST method of update password"+result.hasErrors());
		forgotPasswordControllerLog.trace("no errors userEmail"+user.getUserEmail());
		forgotPasswordControllerLog.trace("no errors userEmail"+user.getUserPassword());
		
		forgotPasswordControllerLog.trace("no errors userEmail"+httpSession.getAttribute("mailId"));
		user.setUserEmail(httpSession.getAttribute("mailId").toString());
		
		if(result.hasErrors()==false) {
			forgotPasswordControllerLog.trace("no errors userEmail"+user.getUserEmail());
			ModelMap map = new ModelMap();
			if(userService.updatePassword(user)==true) {
				userService.deletePasswordUpdatedTokens(user);
				map.put("loginMessage", "Password changed successfully please login");
				map.put("user", new User());
				return new ModelAndView("login",map);
			}
	
		}
		
		return new ModelAndView("updatePassword");

	}
	
}
