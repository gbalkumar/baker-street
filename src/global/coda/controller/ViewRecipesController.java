package global.coda.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import global.coda.model.Recipe;
import global.coda.service.RecipeService;

@Controller
@RestController
@RequestMapping("/recipes")
public class ViewRecipesController {
	private static final Logger viewRecipesLogger = LogManager.getLogger(ViewRecipesController.class.getName());
	
	@Autowired
	RecipeService recipeService;
	
	@GetMapping
	public ModelAndView viewRecipes(HttpSession httpSession) {
		viewRecipesLogger.trace("Inside viewRecipes() Controller function");
		ModelMap map = new ModelMap();
		
		//Check if logged in
		if((httpSession.getAttribute("isAuthenticatedUser"))!=null) {
			viewRecipesLogger.trace("Inside viewRecipes() Controller function--LOGGED IN");

		//Checking if user is Admin
			int userId = (int) httpSession.getAttribute("userId");
	
			if(recipeService.isUserAdmin(userId) == true){
	
				viewRecipesLogger.trace("Inside viewRecipes() Controller function-- ADMIN BLOCK");
	
				List<Recipe> allRecipeList = recipeService.viewAllRecipes(userId);
				viewRecipesLogger.trace("ALL recipe list size:" + allRecipeList.size());
				map.put("recipeList", allRecipeList);
				return new ModelAndView("viewRecipes",map);	
	
			}
			else{
				viewRecipesLogger.trace("Inside viewRecipes() Controller function-- USER BLOCK");
	
				List<Recipe> approvedRecipeList = recipeService.viewAllApprovedRecipes();
				
				//viewRecipesLogger.trace("approved recipe list size:" + approvedRecipeList.size());
				viewRecipesLogger.trace("Session User ID for home page: " + userId);
				map.put("recipeList", approvedRecipeList);
				ModelAndView mav = new ModelAndView("viewRecipes", map);
				return mav;
			}
		}
		else{
			viewRecipesLogger.trace("Inside viewRecipes() Controller function-- NOT LOGGED IN");
			
			List<Recipe> approvedRecipeList = recipeService.viewAllApprovedRecipes();
			
			//viewRecipesLogger.trace("approved recipe list size:" + approvedRecipeList.size());
			map.put("recipeList", approvedRecipeList);
			ModelAndView mav = new ModelAndView("viewRecipes", map);
			return mav;
			
		}
	}
}
