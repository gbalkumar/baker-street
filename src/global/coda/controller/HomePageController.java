package global.coda.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import global.coda.model.Recipe;
import global.coda.service.RecipeService;

@Controller
@RestController
@RequestMapping(value="/")
public class HomePageController {
	
	private static final Logger defaultControllerLogger = LogManager.getLogger(HomePageController.class.getName());
	
	@Autowired
	RecipeService recipeService;

	@GetMapping
	public ModelAndView showRecipes(HttpSession httpSession) {
		defaultControllerLogger.trace("Inside showRecipes() function");
		List<Recipe> topRecipeList = recipeService.viewTopRecipes();
		List<Recipe> mostPopularRecipeList = recipeService.viewMostPopularRecipes();
		ModelMap map = new ModelMap();
		if(httpSession.getAttribute("userId")!=null) {
			int userId = (int) httpSession.getAttribute("userId");
			defaultControllerLogger.trace("Session User ID for home page: " + userId);
			
			List<Recipe> userFavouriteRecipeList = recipeService.viewUserFavouriteRecipes(userId);
			map.put("userFavouriteRecipeList", userFavouriteRecipeList);
		}
		
		List<Recipe> bakerFavouriteRecipeList = recipeService.viewBakersFavouriteRecipes();
		
		map.put("topRecipeList", topRecipeList);
		map.put("mostPopularRecipeList", mostPopularRecipeList);
		
		map.put("bakerFavouriteRecipeList", bakerFavouriteRecipeList);
		//return new ModelAndView("index", "topRecipeList", topRecipeList);
		ModelAndView mav = new ModelAndView("index", map);
		return mav;
	}
}
