package global.coda.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import global.coda.service.RecipeService;

@Controller
@RequestMapping("/images")
public class ImageController {
	private static final Logger imageLogger = LogManager.getLogger(ImageController.class.getName());

	@Autowired
	RecipeService recipeService;
	
	@RequestMapping
	public void testing() {
		imageLogger.trace("Inside testing in image controller");

	}
	
	@RequestMapping(path="/{recipeId}/{imageNumber}", method=RequestMethod.GET)
	public void getImage(HttpServletResponse response, @PathVariable int recipeId, @PathVariable int imageNumber) {
		imageLogger.trace("Path variable recipeId: " + recipeId);
		imageLogger.trace("Path variable Image number: " + imageNumber);
		List<MultipartFile> imageList = recipeService.getImagesForRecipe(recipeId);
		if(imageList != null) {
			
			imageLogger.trace("Image list not null");
			
			
		}
		MultipartFile oneImage = imageList.get(imageNumber);
		try {
			byte[] imageBytes = oneImage.getBytes();
			InputStream inputStream = new ByteArrayInputStream(imageBytes);
			IOUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
