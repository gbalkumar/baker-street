package global.coda.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import global.coda.model.Comment;
import global.coda.model.Ingredient;
import global.coda.model.Instruction;
import global.coda.model.Recipe;
import global.coda.model.Tip;
import global.coda.model.User;
import global.coda.service.RecipeService;

@Controller
@RestController
@RequestMapping("/recipe")
public class RecipeController {
	
	private static final Logger recipeControllerLogger = LogManager.getLogger(RecipeController.class.getName());
	
	@Autowired
	RecipeService recipeService;
	
	@RequestMapping
	public ModelAndView showRecipeForm(HttpSession httpSession, ModelMap model){	
	if((httpSession.getAttribute("isAuthenticatedUser"))!=null) {
		
		List <String> ingredientAutoCompleteList =  new ArrayList<String>();
		List <String> metricAutoCompleteList =  new ArrayList<String>();
		
		ingredientAutoCompleteList = recipeService.ingredientAutoComplete();
		metricAutoCompleteList = recipeService.metricAutoComplete();
		
		//System.out.println("in controller:" +metricAutoCompleteList.size());

		JSONArray ingredientJson = new JSONArray(ingredientAutoCompleteList);
		JSONArray metricJson = new JSONArray(metricAutoCompleteList);

		model.put("jsonIngredientsList", ingredientJson);
		model.put("jsonMetricsList", metricJson);
		
		model.put("ingredientAutoCompleteList", ingredientAutoCompleteList);
		model.put("metricAutoCompleteList", metricAutoCompleteList);
		
		if(!model.containsAttribute("recipe")) {
			recipeControllerLogger.trace("recipe from model attribute there ");
			model.addAttribute("recipe", new Recipe());
		}
		recipeControllerLogger.trace("inside recipeController- showRecipeForm() function");
		return new ModelAndView("createRecipe", model);
		}
	return new ModelAndView("redirect:/user", "user", new User());

	}
	
	
	@RequestMapping (value = "/{recipeId}/addComment", method = RequestMethod.POST)
	public ModelAndView addComment (@ModelAttribute("comment")Comment comment, BindingResult result, ModelMap model, HttpSession httpSession, @PathVariable int recipeId) {
		
		recipeControllerLogger.trace("inside recipeController- addComment() function with recipe ID:" +recipeId);

		
		int userId = (int) httpSession.getAttribute("userId");
		if((httpSession.getAttribute("isAuthenticatedUser"))!=null) {
			recipeControllerLogger.trace("inside recipeController- addComment() function with user Authenticated ");
			comment.setCommentRecipeId(recipeId);
			comment.setCommentUserId(userId);
			recipeService.addComment(comment);
			return new ModelAndView("redirect:/recipe/" +recipeId);
		}
		
		else{
			return new ModelAndView("redirect:/user/login", "user", new User());
		}		
		
	}
	
	@RequestMapping(value="/createRecipe", method = RequestMethod.POST)
	public ModelAndView submitRecipe ( @Valid @ModelAttribute("recipe")Recipe recipe, BindingResult result, ModelMap model,HttpSession httpSession, RedirectAttributes redirectAttributes) {

		recipeControllerLogger.trace("inside recipeController- submitRecipe() function");
		
		if(result.hasErrors() == false){
			
			//int submitterId = recipe.getSubmitterId();
	
			if((httpSession.getAttribute("isAuthenticatedUser"))!=null) {
				
				int userId = (int) httpSession.getAttribute("userId");

				if(recipeService.isPresentUserId(userId) == false){
					recipeControllerLogger.trace("inside recipeController- submitRecipe() function-Not logged in part");
					model.put("message","User ID is not recognised");
					return new ModelAndView("redirect:/recipe",model);
	
				}
				else{
					recipeControllerLogger.trace("inside recipeController- submitRecipe() function- logged in user part");
					/*int recipeId = recipeService.createRecipe(recipe);
					model.addAttribute("message","Recipe has been created");
					model.addAttribute("recipeId",recipeId);
					return new ModelAndView("displayRecipe",model);*/
					List<MultipartFile> inputImages = recipe.getImages();
					
					//recipeController.trace("Images size: " + recipe.getImages().size());
					if(inputImages != null) {
						recipeControllerLogger.trace("Images not null!");
						for(MultipartFile image: recipe.getImages()) {
							recipeControllerLogger.trace("Image name: " + image.getOriginalFilename());
						}
					}
					List<Instruction> instructionList = recipe.getInstructionList();
					if(instructionList != null) {
						recipeControllerLogger.trace("InstructionList not null!");
						for(Instruction instruction: instructionList) {
							recipeControllerLogger.trace("Step: " + instruction.getInstructionStep());
							recipeControllerLogger.trace("Instruction: " + instruction.getInstruction() );
						}
					}
					List<Ingredient> ingredientList = recipe.getIngredientList();
					if(instructionList != null) {
						recipeControllerLogger.trace("ingredientList not null!");
						for(Ingredient ingredient: ingredientList) {
							recipeControllerLogger.trace("Quantity: " + ingredient.getQuantity());
							recipeControllerLogger.trace("Metric: " + ingredient.getMetricName());
							recipeControllerLogger.trace("Ingredient Name: " + ingredient.getIngredientName());
	
						}
					}
					List<Tip> tipList = recipe.getTipList();
					if(tipList != null) {
						recipeControllerLogger.trace("TipList not null!");
						for(Tip tip: tipList) {
							recipeControllerLogger.trace("Tip: " + tip.getTip() );
						}
					}
					recipe.setSubmitterId(userId);
					int recipeId = recipeService.createRecipe(recipe);
					//ModelAndView mav = new ModelAndView("redirect:/recipe/" + recipeId);
					
					//redirects to homepage after recipe creation
					model.put("recipeId", recipeId);
					model.addAttribute("message","Recipe "+recipe.getRecipeName()+ "has been set for approval");
					ModelAndView mav = new ModelAndView("redirect:/");	
					return mav;

				}

			}
			else{
				return new ModelAndView("redirect:/user/login", "user", new User());
			}

		}

		/*model.put("recipe", recipe);
		model.put("result", result);*/
		if(recipe != null) {
			recipeControllerLogger.trace("recipe inside create RECIPE not null ");

		}
		else {
			recipeControllerLogger.trace("recipe inside create RECIPE  null ");

		}
		
		
		//return new ModelAndView("redirect:/recipe", model);
		//return new ModelAndView("forward:/recipe", map);
		
		
	    redirectAttributes.addFlashAttribute("recipe", recipe);
	    redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.recipe", result);
	    return new ModelAndView("redirect:/recipe");

	}
	
	//for ONE RECIPE
	@RequestMapping (value = {"/{recipeId}", "/{recipeId}/{recipeName}"}, method = RequestMethod.GET)
	public ModelAndView viewRecipe(@PathVariable int recipeId, HttpSession httpSession){
		
		recipeControllerLogger.trace("inside recipeController- viewRecipe() function with recipe ID:" +recipeId);
		ModelMap map = new ModelMap();
		
		if((httpSession.getAttribute("isAuthenticatedUser"))==null) {
			recipeControllerLogger.trace("inside recipeController- viewRecipe()--NOT LOGGED IN BLOCK");

			if(recipeService.isRecipeApproved(recipeId) == false){
				map.addAttribute("message", "ERROR RECIPE ID FORBIDDEN");
				return new ModelAndView("errorPage", map);
	
			}
			Recipe recipe = new Recipe();
			recipe= recipeService.viewOneRecipeDetails(recipeId);
			int incrementViewCountStatus = recipeService.incrementViewCountForRecipe(recipeId);
			//boolean isAlreadyFavouriteForUser = recipeService.isAlreadyFavourite(recipeId, userId);
			recipeControllerLogger.trace("IncrementViewCountStatus: " + incrementViewCountStatus);
	
			//System.out.println(recipe.getPreparationTime());
			map.put("recipe", recipe);
			map.put("comment", new Comment());
			ModelAndView mav = new ModelAndView("displayRecipe", map);
			//mav.addObject(recipe);
			
			return mav;
		}
		
		else{
			
			recipeControllerLogger.trace("inside recipeController- viewRecipe()--LOGGED IN BLOCK");

			int userId = (int) httpSession.getAttribute("userId");

			//if user is admin
			if(recipeService.isUserAdmin(userId) == true){
				
				recipeControllerLogger.trace("inside recipeController- viewRecipe()--ADMIN BLOCK");
	
				Recipe recipe = new Recipe();
				recipe= recipeService.viewOneRecipeDetails(recipeId, userId);
				int incrementViewCountStatus = recipeService.incrementViewCountForRecipe(recipeId);
				boolean alreadyFavouriteForUser = recipeService.isAlreadyFavourite(recipeId, userId);
				recipe.setAlreadyFavouriteForUser(alreadyFavouriteForUser);
				recipeControllerLogger.trace("IncrementViewCountStatus: " + incrementViewCountStatus);
	
				//System.out.println(recipe.getPreparationTime());
				map.put("recipe", recipe);
				recipeControllerLogger.trace("inside recipeController- viewRecipe() function with recipe Approval: " +recipe.isApproved());
	
				map.put("comment", new Comment());
				ModelAndView mav = new ModelAndView("displayRecipe", map);
				//mav.addObject(recipe);
				
				return mav;	
			}
			
			else{
				
				recipeControllerLogger.trace("inside recipeController- viewRecipe()--USER BLOCK");
	
				if(recipeService.isRecipeApproved(recipeId) == false){
					map.addAttribute("message", "ERROR RECIPE ID FORBIDDEN");
					return new ModelAndView("errorPage", map);
		
				}
				Recipe recipe = new Recipe();
				recipe= recipeService.viewOneRecipeDetails(recipeId, userId);
				int incrementViewCountStatus = recipeService.incrementViewCountForRecipe(recipeId);
				recipeControllerLogger.trace("IncrementViewCountStatus: " + incrementViewCountStatus);
				boolean alreadyFavouriteForUser = recipeService.isAlreadyFavourite(recipeId, userId);
				recipe.setAlreadyFavouriteForUser(alreadyFavouriteForUser);
				//System.out.println(recipe.getPreparationTime());
				map.put("recipe", recipe);
				map.put("comment", new Comment());
				ModelAndView mav = new ModelAndView("displayRecipe", map);
				//mav.addObject(recipe);
				
				return mav;
			}
		}

	}
	
	@RequestMapping(value = "/{recipeId}/addToFavourite", method = RequestMethod.POST)
	public ModelAndView addToFavourites(@ModelAttribute("comment")Comment comment, BindingResult result, HttpSession httpSession, @PathVariable int recipeId) {
		
		recipeControllerLogger.trace("inside recipeController- addToFavourites() function with recipe ID:" +recipeId);

		if((httpSession.getAttribute("isAuthenticatedUser"))!=null) {
			int userId = (int) httpSession.getAttribute("userId");

			recipeControllerLogger.trace("inside recipeController- addToFavourites() function with user Authenticated ");
			
			if(recipeService.isAlreadyFavourite(recipeId, userId)==false){
				boolean status = recipeService.addToFavourite(recipeId, userId);
				
				if(status == true){
					recipeControllerLogger.trace("inside recipeController- addToFavourites() functionRedirect homepage");
					return new ModelAndView("redirect:/");
				}
				else{
					recipeControllerLogger.trace("inside recipeController- addToFavourites() functionRedirect DISPLAYRECIPE");
					return new ModelAndView("displayRecipe");
				}
			}
			else {
				recipeControllerLogger.trace("inside recipeController- addToFavourites() function recipe already a favourite");
				return new ModelAndView("redirect:/");

			}
		}
		else{
			return new ModelAndView("redirect:/user/login", "user", new User());
		}			
		
	}
	
	
	@RequestMapping(value = "/{recipeId}/removeFromFavourite", method = RequestMethod.POST)
	public ModelAndView removeFromFavourites(@ModelAttribute("comment")Comment comment, BindingResult result, HttpSession httpSession, @PathVariable int recipeId) {
		
		recipeControllerLogger.trace("inside recipeController- removeFromFavourites() function with recipe ID:" +recipeId);

		if((httpSession.getAttribute("isAuthenticatedUser"))!=null) {
			int userId = (int) httpSession.getAttribute("userId");

			recipeControllerLogger.trace("inside recipeController- removeFromFavourites() function with user Authenticated ");
			
			boolean status = recipeService.removeFromFavourite(recipeId, userId);
			if(status == true){
				recipeControllerLogger.trace("inside recipeController- removeFromFavourites() functionRedirect homepage");
				return new ModelAndView("redirect:/");
			}
			else{
				recipeControllerLogger.trace("inside recipeController- removeFromFavourites() functionRedirect DISPLAYRECIPE");
				return new ModelAndView("displayRecipe");
			}
		}
		else{
			return new ModelAndView("redirect:/user/login", "user", new User());
		}			
		
	}
	
	@RequestMapping(value = "/{recipeId}/approveRecipe", method = RequestMethod.POST)
	public ModelAndView approveRecipe(HttpSession httpSession, @PathVariable int recipeId){
		
		recipeControllerLogger.trace("inside recipeController- approveRecipe() function with recipe ID:" +recipeId);
		int userId = (int) httpSession.getAttribute("userId");
		ModelMap map = new ModelMap();

		if((httpSession.getAttribute("isAuthenticatedUser"))!=null) {
			recipeControllerLogger.trace("inside recipeController- approveRecipe() function authenticated if loop");
			boolean approveStatus = recipeService.approveRecipe(recipeId, userId);
			if(approveStatus == true){

				recipeControllerLogger.trace("inside recipeController- approveRecipe() function STATUS : "+approveStatus);

				return new ModelAndView("redirect:/");
			}
			else{
				map.addAttribute("message", "ERROR RECIPE ID FORBIDDEN");
				return new ModelAndView("errorPage", map);
			}
			
		}	
		else{
			return new ModelAndView("redirect:/user/login", "user", new User());
		}
		
	}
	
	@PostMapping(value = "/{recipeId}/rating")
	public ModelAndView createUserRating(@PathVariable int recipeId, @RequestParam("rating") double rating, HttpSession httpSession) {
		recipeControllerLogger.trace("inside recipeController- createUserRating() function with recipe ID:" +recipeId);
		ModelAndView mav = new ModelAndView("redirect:/");
		int userId = (int)httpSession.getAttribute("userId");
		recipeControllerLogger.trace("Session User ID for rating: " + userId);

		int ratingStatus = recipeService.createUserRecipeRating(recipeId, userId, rating);
		recipeControllerLogger.trace("Rating updation status: " + ratingStatus);
		return mav;
		
	}
	
}