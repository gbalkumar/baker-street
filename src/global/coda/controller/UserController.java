package global.coda.controller;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import global.coda.model.Recipe;
import global.coda.model.User;
import global.coda.service.RecipeService;
import global.coda.service.UserService;

@Controller
@RestController
@RequestMapping("/user")
public class UserController {
	private static final Logger userControllerLog = LogManager.getLogger(UserController.class.getName());
	
	ModelMap map;

	//UserService userService = new UserServiceImpl();
	@Autowired
	UserService userService;
	
	@Autowired
	RecipeService recipeService;
	
	@Autowired 
	 private HttpSession httpSession;
	

	public HttpSession getHttpSession() {
		return this.httpSession;
	}

	public void setHttpSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}
	

	@RequestMapping(value="/login", method = RequestMethod.GET)
	public ModelAndView login(HttpSession httpSession) {
		userControllerLog.trace(httpSession.isNew());
		userControllerLog.trace(  System.getProperty("APP_HOSTNAME"));
		userControllerLog.trace(  System.getProperty("SENDGRID_API_KEY"));
		userControllerLog.trace(System.getProperty("TEST_VAR"));
		userControllerLog.trace(System.getProperty("APP_HOSTNAME"));
		userControllerLog.trace("inside /user login");
		map = new ModelMap();
		if(httpSession.getAttribute("isAuthenticatedUser")==null) {
			return new ModelAndView("login", "user", new User());

		}else {
			map.put("status",  "0");
			return new ModelAndView("redirect:/",map);
			
		}

		
	}
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public ModelAndView createUser() {
		return new ModelAndView("createUser", "user", new User());
	}
	
	@RequestMapping(value="/forgot-password",method=RequestMethod.GET)
	public ModelAndView forgotPassword() {
		userControllerLog.trace("inside forgot password GET");

		return new ModelAndView("forgotPassword", "user", new User());
	}
	
	@RequestMapping(value="/profile",method=RequestMethod.GET)
	public ModelAndView profile(HttpSession httpSession) {
		userControllerLog.trace("inside profiel view GET");

		map = new ModelMap();
		User user = new User();

		user = userService.userProfile(httpSession.getAttribute("userEmail").toString());

		if(user.getUserName()==null|| user.getPhoneNumber()==null || user.getUserAddress()==null|| user.getWebURL()==null) {

			userControllerLog.trace("inside profiel view GET prile not complete error message");

			map.put("profileNotComplete", "Please Complete the Profile");	
		}


		map.put("user", user);
		return new ModelAndView("viewProfile",map);
	}
	
	@RequestMapping(value="/profile/edit", method = RequestMethod.GET)
	public ModelAndView updateProfile(HttpSession httpSession) {
		if(httpSession.getAttribute("isAuthenticatedUser")!=null) {
			User user = new User();
			user = userService.userProfile(httpSession.getAttribute("userEmail").toString());
			return new ModelAndView("updateProfile", "user", user);
		} else {
			return new ModelAndView("login","user",new User());
		}

	}
	
	@RequestMapping(value="/register", method = RequestMethod.POST)
	public ModelAndView createUser(@Valid @ModelAttribute("user")User user,BindingResult result, ModelMap model, HttpSession httpSession) {
		userControllerLog.trace("inside /user/createUser  POST");
		map = new ModelMap();
		
		if(result.hasErrors()==false) {
			
			//used to check whether to display the span element in updateProfile.jsp
			map.put("errorStatus", true);

			if(userService.isUserValidated(user)==false) {
				userControllerLog.trace("inside /user/createUser  POST  already in validation table");

				map.put("createUserMessage",  "A Verification link is already sent to your mail, Please Verify it");
				map.put("user", new User());
				return new ModelAndView("createUser",map);
			} 
			userControllerLog.trace("inside /user/createUser  POST  not in validation table user can be created");

			if(userService.isUserExists(user)==false) {
				userControllerLog.trace("inside /user/createUser  POST  not in validation table user can be created");

				//int userId=userService.createUser(user);
				//if(userId>0) {
				userService.generateUserValidationKey(user);
				//}
				userControllerLog.trace("user not exists user cretaed");
				map.put("loginMessage","Please verify the link that has been sent to your Email");
				map.put("user",new User());
				return new ModelAndView("login",map);
			} else {
				
				map.put("createUserMessage", "You have already registered with this EMail please login to continue");
				map.put("user", new User());
				return new ModelAndView("createUser",map);
			}
		}
		else{
			map.put("errorStatus", false);
			return new ModelAndView("createUser",map);
		}


	}

	
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public ModelAndView login(@Valid @ModelAttribute("user")User user,BindingResult result, ModelMap model, HttpSession httpSession) {
		userControllerLog.trace("inside /user login post");
		map = new ModelMap();
		if((httpSession.getAttribute("isAuthenticatedUser"))==null) {
			if(result.hasErrors()==false) {
				
				if(userService.isUserValidated(user)==false) {
					map.put("loginMessage","Please Verify your account before you proceed to login");
					return new ModelAndView("login",map);
				} 
				
				
				userControllerLog.trace("inside /user login userEmail"+user.getUserEmail());
				userControllerLog.trace("inside /user login userEmail"+user.getUserPassword());
				boolean isAuthenticated =false;
				
				
				 int userId = userService.loginUser(user);
				 if(userId>0) {
					 isAuthenticated =true;
				 }
				 
				userControllerLog.trace("inside /user login get"+isAuthenticated);
				if(isAuthenticated) {
					userControllerLog.trace(httpSession.getCreationTime());
					map.put("status", "1");
					httpSession.setAttribute("userEmail",user.getUserEmail());
					httpSession.setAttribute("isAuthenticatedUser",true);
					httpSession.setAttribute("userId",userId);
					userControllerLog.trace("inside /user login ====="+userId);
					boolean isAdmin = userService.isAdmin(user.getUserEmail());
					userControllerLog.trace("inside /user login ====="+isAdmin);
					httpSession.setAttribute("isAdmin",isAdmin);
					userControllerLog.trace("username"+httpSession.getAttribute("userEmail")+"login"+httpSession.getAttribute("isAuthenticatedUser")+httpSession.getAttribute("userId")+httpSession.getAttribute("isAdmin"));

					return new ModelAndView("redirect:/",map);
				} else if(!isAuthenticated){
					userControllerLog.trace("inside /user login else part");
					map.put("loginMessage","Email or Password is wrong");
					return new ModelAndView("login",map);
				}
			}
			
		}
		else {
			map.put("loginMessage",  "you already logged in");
			return new ModelAndView("redirect:/",map);

		}
		
		userControllerLog.trace("inside /user login error in fields");

		return new ModelAndView("login");

	}
	
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public ModelAndView logout(@ModelAttribute("user")User user,BindingResult result,ModelAndView model,SessionStatus status, HttpSession httpSession,HttpServletRequest request ) {

		
//		if(httpSession.getAttribute("isAuthenticatedUser")!=null) {
			try {
				httpSession=request.getSession(false);
			userControllerLog.trace("inside logout");
			userControllerLog.trace("username"+httpSession.getAttribute("userEmail")+"login"+httpSession.getAttribute("isAuthenticatedUser")+httpSession.getAttribute("userId")+httpSession.getAttribute("isAdmin"));
			httpSession.removeAttribute("userEmail");
			httpSession.removeAttribute("isAuthenticatedUser");
			httpSession.removeAttribute("userId");
			httpSession.removeAttribute("isAdmin");
			userControllerLog.trace("username"+httpSession.getAttribute("userEmail")+"login"+httpSession.getAttribute("isAuthenticatedUser")+httpSession.getAttribute("userId")+httpSession.getAttribute("isAdmin"));
			httpSession.invalidate();
			Cookie[] cookies = request.getCookies();
			for (Cookie cookie : cookies) {
			cookie.setMaxAge(0);
			cookie.setValue(null);
			cookie.setPath("/");
			}
			
			Cookie cookie = new Cookie("JSESSIONID","");
			cookie.setValue(null);
			cookie.setMaxAge(0);
			cookie.setPath("/");
			status.setComplete();
			userControllerLog.trace("session invalidated logout");
			model.clear();
			//return "login";
			return new ModelAndView("redirect:/");
			}
			catch (Exception e){
				e.printStackTrace();
				Cookie[] cookies = request.getCookies();
				for (Cookie cookie : cookies) {
				cookie.setMaxAge(0);
				cookie.setValue(null);
				cookie.setPath("/");
				}
				return new ModelAndView("redirect:/");
			}
			//}
	
	}
	
	
	@RequestMapping(value="/forgot-password",method=RequestMethod.POST)
	public ModelAndView forgotPasword(@Valid @ModelAttribute("user")User user,BindingResult result,ModelAndView model,HttpSession httpSession) {
		map = new ModelMap();
		userControllerLog.trace("inside forgot password post http session passed"+result.hasErrors());
		userControllerLog.trace("inside forgot password post http session passed"+user.getUserEmail());

		if(httpSession.getAttribute("isAuthenticatedUser")==null) {
				if(result.hasErrors()==false) {
					
					userControllerLog.trace("inside forgot password post http session passed");

					if(userService.isUserExists(user)==true) {
						userControllerLog.trace("inside forgot password post user exists");
							userService.deleteExpiredPassowrdTokens();
							
							if(userService.isAlreadyRequested(user)==true) {
								map.put("forgotPasswordMessage","An email has been sent to your mail already, please click on the link and change the password, Link expires in 30 minutes");
								return new ModelAndView("forgotPassword",map);
								
							} 
							else {
								userControllerLog.trace("inside forgot password post user exists and not requested for password change");
		
								boolean status=userService.generateForgotPasswordLink(user);
								if(status) {
									map.put("forgotPasswordMessage","The Password reset link has been sent to the mail :");
									map.put("email", user.getUserEmail());

								} else {
									map.put("forgotPasswordMessage","Sorry An error has occured, please try again after a few minutes");

								}
								return new ModelAndView("forgotPassword",map);
							}
						
					} 
					else {
						userControllerLog.trace("inside forgot password post user not exists");

						map.put("forgotPasswordMessage","The Password reset link has been sent to the mail :");
						map.put("email", user.getUserEmail());
						map.put("user", new User());
						return new ModelAndView("forgotPassword", map);
					}
					
					
				} else {
				
					return new ModelAndView("forgotPassword","user",user);
					
				}
			
		}
	return null;
   }
	
	@RequestMapping(value="/profile/edit", method = RequestMethod.POST)
	public ModelAndView updateProfile(@ModelAttribute("user")User user,BindingResult result,ModelAndView model,HttpSession httpSession ) {
		
		
		if(result.hasErrors()==true) {
			userControllerLog.trace("inside profile EDIT error");
			return new ModelAndView("redirect:/user/profile/edit","user",user);
		}
		else{
			userControllerLog.trace("inside profile EDIT no error");
			userService.updateProfile((int)httpSession.getAttribute("userId"), user);	
			return new ModelAndView("redirect:/user/profile");
		}
	}
	
	@GetMapping(value="/favourite")
	public ModelAndView viewFavouriteRecipes(HttpSession httpSession) {
		map = new ModelMap();
		if(httpSession.getAttribute("userId")!=null) {
			int userId = (int) httpSession.getAttribute("userId");
			userControllerLog.trace("Session User ID for home page: " + userId);
			
			List<Recipe> userFavouriteRecipeList = recipeService.viewUserFavouriteRecipesAllDetails(userId);
			//map.put("userFavouriteRecipeList", userFavouriteRecipeList);
			map.put("recipeList", userFavouriteRecipeList);
			map.put("userFavourite", true);
			return new ModelAndView("viewRecipes", map);
		}
		else {
			map.put("message", "You shouldn't be here :P");
			return new ModelAndView("errorPage", map);
		}
	}
	
	@GetMapping(value="/approve")
	public ModelAndView viewUnapprovedRecipes(HttpSession httpSession) {
		map = new ModelMap();
		if(httpSession.getAttribute("userId")!=null) {
			if(httpSession.getAttribute("isAdmin") != null) {
				if((boolean) httpSession.getAttribute("isAdmin") == true) {
					int userId = (int) httpSession.getAttribute("userId");
					userControllerLog.trace("Session User ID for home page: " + userId);
					
					List<Recipe> unapprovedRecipeList = recipeService.viewAllUnapprovedRecipes(userId);
					//map.put("userFavouriteRecipeList", userFavouriteRecipeList);
					map.put("recipeList", unapprovedRecipeList);
					map.put("adminUnapproved", true);
					return new ModelAndView("viewRecipes", map);
				}
				else {
					map.put("message", "Access denied: You are not an admin!");
					return new ModelAndView("errorPage", map);
				}
			}
			else {
				userControllerLog.trace("Some weird thing happened. Admin null, but user logged in");
				map.put("message", "Access denied! Not an admin");
				return new ModelAndView("errorPage", map);
			}
			
			
		}
	
		else {
			map.put("message", "You shouldn't be here :P");
			return new ModelAndView("errorPage", map);
		}
		
		
		
	}
	
}
