package global.coda.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import global.coda.model.Message;
import global.coda.service.MessageService;

@Controller
@RequestMapping("/message")
public class MessageController {
	private static final Logger messageControllerLog = LogManager.getLogger(MessageController.class.getName());

	@Autowired
	MessageService messageService;
	
	@RequestMapping
	public ModelAndView messageAdmin(HttpSession httpSession) {
		messageControllerLog.trace("inside message controller get");
		if(httpSession.getAttribute("isAuthenticatedUser")!=null) {
			messageControllerLog.trace("inside message controller get in session"+ httpSession.getAttribute("userEmail"));
			ModelMap map = new ModelMap();
			map.put("userEmail", httpSession.getAttribute("userEmail"));
			map.put("message", new Message()); 
			return new ModelAndView("messageToAdmin",map);
		}
		
		return new ModelAndView("homePage");
	}
	
	@RequestMapping(value="/send", method = RequestMethod.POST)
	public ModelAndView messageAdmin(@Valid @ModelAttribute("message")Message message,BindingResult result, ModelMap model,HttpSession httpSession) {
		ModelMap map = new ModelMap();
		messageControllerLog.trace("inside message controlle rpost");
		if(result.hasErrors()==false) {
			boolean status = messageService.send(message);
			if(status) {
				map.put("status", "Message Sent Successfully");
				map.put("message", new Message());
				return new ModelAndView("messageToAdmin",map);	
			}
			
		}
		
		map.put("status", "Sorry an Error has Encountered");

		return new ModelAndView("messageToAdmin",map);
	}
	
	
}
