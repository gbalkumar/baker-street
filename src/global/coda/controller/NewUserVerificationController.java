package global.coda.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import global.coda.model.User;
import global.coda.service.UserService;

@Controller
@RequestMapping(value="/account-verify")
public class NewUserVerificationController {
	
	private static final Logger newUserVerificationControllerLog = LogManager.getLogger(ChangePasswordController.class.getName());

	@Autowired
	UserService userService;
	
	@GetMapping
	public ModelAndView updatePassword(@RequestParam String token,HttpSession httpSession) {
		ModelMap map= new ModelMap();
		
		newUserVerificationControllerLog.trace(token);
		String mailId= userService.isUserTokenValid(token);
		newUserVerificationControllerLog.trace("mailId returned"+mailId);

		if(mailId!=null) {
			newUserVerificationControllerLog.trace("inside  new User controller GET , token is valid");
			
			httpSession.setAttribute("mailId",mailId);
			int userId=userService.createUser(mailId);
			if(userId>0){
				userService.deleteVerifiedUserTokens(mailId);
			}
			map.put("user",new User());
			map.put("loginMessage", "Hey, your account has been verified successfully, please login to continue");
			return new ModelAndView("login",map);	
		}
		
		return new ModelAndView("errorPage");			
	}
	
	
}
