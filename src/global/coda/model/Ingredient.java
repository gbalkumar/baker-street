package global.coda.model;

import java.util.Map;

public class Ingredient {
	
	private int ingredientId;
	private String ingredientName;
	private String metricName;
	private int quantity;
	
	public Ingredient(){
		
	}
	
	public Ingredient(Map<String, Object> map) {
		
		setIngredientName((String)map.get("ingredientName"));
		setMetricName((String) map.get("metricName"));
		setQuantity((int) map.get("quantity"));		
	}

	public int getIngredientId() {
		return ingredientId;
	}

	public void setIngredientId(int ingredientId) {
		this.ingredientId = ingredientId;
	}

	public String getIngredientName() {
		return ingredientName;
	}

	public void setIngredientName(String ingredientName) {
		this.ingredientName = ingredientName;
	}

	public String getMetricName() {
		return metricName;
	}

	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
	
	


}
