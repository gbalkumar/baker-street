package global.coda.model;

import java.util.Map;

public class Tip {
	private int recipeId;
	private String tip;
	
	
	public Tip() {
		
	}
	public Tip(Map<String, Object> map) {
		
		//setRecipeId((int)map.get("recipeId"));
		setTip((String)map.get("tip"));
	
	}
	
	
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	
	
}
