package global.coda.model;

import java.util.Map;

public class Instruction {
	
	private int recipeId;
	private int instructionStep;
	private String instruction;
	
	public Instruction() {
		
	}
	
	public Instruction(Map<String, Object> map) {
		
		//setRecipeId((int)map.get("recipeId"));
		setInstructionStep((int)map.get("instructionStep"));
		setInstruction((String)map.get("instruction"));
	
	}
	
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public int getInstructionStep() {
		return instructionStep;
	}

	public void setInstructionStep(int instructionStep) {
		this.instructionStep = instructionStep;
	}

	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instructionLine) {
		this.instruction = instructionLine;
	}
	
	

}
