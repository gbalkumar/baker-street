package global.coda.model;



import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;



public class User {
	
	@NotEmpty
	@Email
	private String userEmail;	
	
	@Pattern(regexp="^(?=.{6,15})(?=.*[A-Z])(?=.*[0-9]).*$")
	private String userPassword;
	
	//@Pattern(regexp="(^\\s*$)|(^\\d{10}$)")
	//@Pattern(regexp = "/^\\d{10}$/")
	//@Pattern(regexp = "null")
	@Size(min=2)
	private String phoneNumber=null;
	
	private String userAddress = null;
	private String userName = null;	
	private String webURL = null;
	boolean isAdmin ;
	

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public boolean isAdmin() {
		return isAdmin;
	}
	
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserAddress() {
		return userAddress;
	}
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getWebURL() {
		return webURL;
	}
	public void setWebURL(String webURL) {
		this.webURL = webURL;
	}

	

}
