package global.coda.model;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
 
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class Message implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String fromEmail;
	@NotEmpty
	private String subject;
	@NotEmpty
	private String body;
	
	private String toEmail="bakerstreetassistance@gmail.com";
	
	
	
	
	
	public String getToEmail() {
		return toEmail;
	}
	
	public Message(){
		
	}
	public String getFromEmail() {
		return fromEmail;
	}
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	
	
	
	
}
