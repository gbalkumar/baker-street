package global.coda.model;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;


public class Recipe {

	private int recipeId;
	private int styleId;
	private boolean approved;
	private int readyInTime;
	private int submitterId;
	private String submitterEmail;

	private int viewCount;
	private double averageRating;

	@NotEmpty
	private String recipeName;
	
	@NotEmpty
	private String recipeHeading;
	
	@NotEmpty
	private String styleName;
	
	@NotNull
	@Min(1)
	private int preparationTime;
	
	@Min(1)
	private int cookTime;
	
	//from other models
	@NotEmpty
	private List<Instruction> instructionList;
	
	@NotEmpty
	private List <Ingredient> ingredientList;

	private List<Comment> commentList;
	
	private List<Tip> tipList;


	private List<Picture> pictureList;
	
	private List<MultipartFile> images;
	
	private boolean alreadyFavouriteForUser;
	public Recipe() {
	
	}
	public Recipe( Map<String,Object> map, int x){
		
		setRecipeName((String) map.get("recipeName"));
		setSubmitterId((int) map.get("submitterId"));
		setStyleId((int) map.get("styleId"));
		setRecipeHeading((String) map.get("recipeHeading"));
		setPreparationTime((int) map.get("preparationTime"));
		setCookTime((int) map.get("cookTime"));
		setReadyInTime((int) map.get("readyInTime"));
		setRecipeId((int) map.get("recipeId"));
		setApproved((boolean) map.get("isApproved"));
		
	}
	
	public Recipe(Map<String, Object> map) {
		setRecipeId((int) map.get("recipeId"));
		setRecipeName((String) map.get("recipeName"));
		setStyleName((String) map.get("styleName"));
		setSubmitterEmail((String) map.get("submitterEmail"));
		setViewCount((int) map.get("viewCount"));
		setAverageRating((double) map.get("averageRating"));
	}
	
	public String getSubmitterEmail() {
		return submitterEmail;
	}

	public void setSubmitterEmail(String submitterEmail) {
		this.submitterEmail = submitterEmail;
	}
	public int getPreparationTime() {
		return preparationTime;
	}
	public void setPreparationTime(int preparationTime) {
		this.preparationTime = preparationTime;
	}
	public int getCookTime() {
		return cookTime;
	}
	public void setCookTime(int cookTime) {
		this.cookTime = cookTime;
	}
	public int getReadyInTime() {
		return readyInTime;
	}
	public void setReadyInTime(int readyInTime) {
		this.readyInTime = readyInTime;
	}
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public String getRecipeHeading() {
		return recipeHeading;
	}
	public void setRecipeHeading(String recipeHeading) {
		this.recipeHeading = recipeHeading;
	}
	
	public String getStyleName() {
		return styleName;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}
	public int getStyleId() {
		return styleId;
	}
	public void setStyleId(int styleId) {
		this.styleId = styleId;
	}
	
	public List<Comment> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}
	public int getSubmitterId() {
		return submitterId;
	}
	public void setSubmitterId(int submitterId) {
		this.submitterId = submitterId;
	}
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	public double getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}
	public List<Picture> getPictureList() {
		return pictureList;
	}
	public void setPictureList(List<Picture> pictureList) {
		this.pictureList = pictureList;
	}
	public List<Instruction> getInstructionList() {
		return instructionList;
	}
	public void setInstructionList(List<Instruction> instructionList) {
		this.instructionList = instructionList;
	}
	public List<Ingredient> getIngredientList() {
		return ingredientList;
	}
	public void setIngredientList(List<Ingredient> ingredientList) {
		this.ingredientList = ingredientList;
	}

	public List<MultipartFile> getImages() {
		return images;
	}
	public void setImages(List<MultipartFile> images) {
		this.images = images;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	public boolean isAlreadyFavouriteForUser() {
		return alreadyFavouriteForUser;
	}
	public void setAlreadyFavouriteForUser(boolean alreadyFavouriteForUser) {
		this.alreadyFavouriteForUser = alreadyFavouriteForUser;
	}
	public List<Tip> getTipList() {
		return tipList;
	}
	public void setTipList(List<Tip> tipList) {
		this.tipList = tipList;
	}
	
	
}
