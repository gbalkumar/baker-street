package global.coda.model;

import java.sql.Timestamp;
import java.util.Map;

public class Comment {
	
	private int commentId;
	private String comment;
	private Timestamp commentTime;
	private int commentUserId;
	private String commentUserEmail;
	private int commentRecipeId;
	private int commentYear;
	private int commentDay;
	private int commentHour;
	private int commentMinute;
	private int commentSecond;
	
	public Comment() {
		
	}
	
	public Comment(Map<String, Object> map) {
		this.comment = (String) map.get("comment");
		this.commentUserEmail = (String) map.get("commentUserEmail");
		this.commentTime = (Timestamp)map.get("commentTime");
	}
	
	
	
	public int getCommentHour() {
		return commentHour;
	}

	public void setCommentHour(int commentHour) {
		this.commentHour = commentHour;
	}

	public int getCommentYear() {
		return commentYear;
	}

	public void setCommentYear(int commentYear) {
		this.commentYear = commentYear;
	}

	public int getCommentDay() {
		return commentDay;
	}

	public void setCommentDay(int commentDay) {
		this.commentDay = commentDay;
	}

	public int getCommentMinute() {
		return commentMinute;
	}

	public void setCommentMinute(int commentMinute) {
		this.commentMinute = commentMinute;
	}

	public int getCommentSecond() {
		return commentSecond;
	}

	public void setCommentSecond(int commentSecond) {
		this.commentSecond = commentSecond;
	}

	public String getCommentUserEmail() {
		return commentUserEmail;
	}

	public void setCommentUserEmail(String commentUserEmail) {
		this.commentUserEmail = commentUserEmail;
	}

	public Timestamp getCommentTime() {
		return commentTime;
	}
	public void setCommentTime(Timestamp commentTime) {
		this.commentTime = commentTime;
	}
	public int getCommentUserId() {
		return commentUserId;
	}
	public void setCommentUserId(int commentUserId) {
		this.commentUserId = commentUserId;
	}
	public int getCommentRecipeId() {
		return commentRecipeId;
	}
	public void setCommentRecipeId(int commentRecipeId) {
		this.commentRecipeId = commentRecipeId;
	}
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	

}
