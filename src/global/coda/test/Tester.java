package global.coda.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import global.coda.dao.RecipeDAO;
import global.coda.model.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={
"classpath:/WEB-INF/applicationContext.xml"})
public class Tester {
	private static RecipeDAO recipeDAO;
	Map<String, Object> commentMap1;
	
	@BeforeClass
	public void setup() {
		//ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());

		//recipeDAO = context.getBean("recipeDAO", RecipeDAO.class);
		commentMap1 = new HashMap<String, Object>();
		commentMap1.put("commentRecipeId", 1);
		commentMap1.put("commentUserId", 1);
		commentMap1.put("comment", "Great recipe! Comment 1");
	}
	
	@Test
	public void testCommentInsertion() {
		Comment comment1 = new Comment(commentMap1);
		
	}
}
