package global.coda.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import global.coda.model.Comment;
import global.coda.model.Ingredient;
import global.coda.model.Instruction;
import global.coda.model.Recipe;
import global.coda.model.Tip;

public class RecipeMapper {
	
	private static final Logger recipeMapperLog = LogManager.getLogger(RecipeMapper.class.getName());

	public List<Recipe> viewAllRecipes (ResultSet resultSet){
		
		recipeMapperLog.trace("INSIDE RecipeMapper - viewRecipe() function");

		
		Map<String,Object> map = new HashMap<String,Object>();
		List<Recipe> recipeList = new ArrayList<Recipe>();
		
		try{
			
			while(resultSet.next()){
	
				map.put("recipeName", resultSet.getString("recipe_name"));
				map.put("recipeId", resultSet.getInt("recipe_id"));
				map.put("styleName", resultSet.getString("style_name"));
				map.put("submitterEmail", resultSet.getString("user_email"));
				map.put("viewCount", resultSet.getInt("view_count"));
				map.put("averageRating", resultSet.getDouble("average_rating"));
				Recipe recipe = new Recipe(map);
				recipe.setApproved(resultSet.getBoolean("is_approved"));
				recipeList.add(recipe);
			}
			
		} catch( SQLException e){
			e.printStackTrace();
		}
		
		return recipeList;
		
	}
	
	public Recipe viewOneRecipeDetails (ResultSet resultSet){
		
		recipeMapperLog.trace("INSIDE RecipeMapper - viewOneRecipeDetails() function");	
		Map<String,Object> map = new HashMap<String,Object>();
		
		try{
			
			while(resultSet.next()){
				
				map.put("recipeName", resultSet.getString("recipe_name"));
				map.put("submitterId", resultSet.getInt("submitter_id"));
				map.put("styleId", resultSet.getInt("style_id"));
				map.put("recipeHeading", resultSet.getString("heading"));
				map.put("preparationTime", resultSet.getInt("preparation_time"));
				map.put("cookTime", resultSet.getInt("cook_time"));
				map.put("readyInTime", resultSet.getInt("readyin_time"));
				map.put("recipeId", resultSet.getInt("recipe_id"));
				map.put("recipeName", resultSet.getString("recipe_name"));
				map.put("isApproved", resultSet.getBoolean("is_approved"));
				
				Recipe recipe = new Recipe(map, 1);
				recipe.setApproved(resultSet.getBoolean("is_approved"));
				recipeMapperLog.trace("INSIDE RecipeMapper - viewOneRecipeDetails() function APPROVE STATUS:" +resultSet.getBoolean("is_approved"));	


				return recipe;
			}
			
		} catch( SQLException e){
			e.printStackTrace();
			return null;
		}
		return null;
		
	
		
	}
	
	public List<Instruction> getInstruction (ResultSet resultSet){
		
		recipeMapperLog.trace("INSIDE RecipeMapper - getInstruction() function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Instruction> instructionList = new ArrayList<Instruction>();
		try {
			while(resultSet.next()){
				
				map.put("instructionStep", resultSet.getInt("step"));
				map.put("instruction", resultSet.getString("instruction"));
				
				Instruction instruction = new Instruction(map);
				instructionList.add(instruction);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}


		return instructionList;
		
		
	}
	
	public void getCommentTime (ResultSet resultSet, Comment comment){
		
		recipeMapperLog.trace("INSIDE RecipeMapper - getCommentTime() function");
		try{
			while(resultSet.next()){
				
				comment.setCommentYear((int)resultSet.getInt(1));
				comment.setCommentDay((int)resultSet.getInt(2));
				comment.setCommentHour((int)resultSet.getInt(3));
				comment.setCommentMinute((int)resultSet.getInt(4));
				comment.setCommentSecond((int)resultSet.getInt(5));

			}

		}catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public List<Comment> getComment (ResultSet resultSet){
		
		recipeMapperLog.trace("INSIDE RecipeMapper - getComment() function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Comment> commentList = new ArrayList<Comment>();
		try {
			while(resultSet.next()){
				
				map.put("comment", resultSet.getString("comment"));
				map.put("commentUserEmail", resultSet.getString("user_email"));
				map.put("commentTime", resultSet.getTimestamp("comment_submitted_time"));

				Comment comment = new Comment(map);
				comment.setCommentId(resultSet.getInt("comment_id"));
				commentList.add(comment);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}


		return commentList;
		
		
	}
	
	public List<Ingredient> getIngredient (ResultSet resultSet){
		
		recipeMapperLog.trace("INSIDE RecipeMapper - getInstruction() function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Ingredient> ingredientList = new ArrayList<Ingredient>();
		try {
			while(resultSet.next()){
				
				map.put("quantity", resultSet.getInt("quantity"));
				map.put("metricName", resultSet.getString("metric_name"));
				map.put("ingredientName", resultSet.getString("ingredient_name"));

				Ingredient ingredient = new Ingredient(map);
				ingredientList.add(ingredient);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}


		return ingredientList;
		
		
	}
	/*public List<Recipe> viewTopRecipes(ResultSet rs) {
		List<Recipe> recipeList = new ArrayList<Recipe>();
		try {
			String previousRecipeName = "";
			List<Picture> pictureList = null;
			Recipe recipe = null;
			int counter = 0;
			while(rs.next()) {
				//Recipe recipe = new Recipe();
				String recipeName = rs.getString("recipe_name");
				if(recipeName.equals(previousRecipeName) == false) {
					if(counter != 0) {
						recipe.setPictureList(pictureList);
						recipeList.add(recipe);
					}
					recipe = new Recipe();
					recipe.setRecipeName(rs.getString("recipe_name"));
					recipe.setRecipeId(rs.getInt("recipe_id"));
					pictureList = new ArrayList<Picture>();
				}
				Picture picture = new Picture();
				picture.setPicture(rs.getBytes("image"));
				picture.setPictureId(rs.getInt("image_id"));
				pictureList.add(picture);
			
				counter++;	
			}
			
			return recipeList;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}

		return null;
	}*/

	
	/*public List<Recipe> viewTopRecipes(ResultSet rs) {
		List<Recipe> recipeList = new ArrayList<Recipe>();
		try {
			String previousRecipeName = "";
			List<MultipartFile> images = null;
			Recipe recipe = null;
			int counter = 0;
			while(rs.next()) {
				//Recipe recipe = new Recipe();
				String recipeName = rs.getString("recipe_name");
				if(recipeName.equals(previousRecipeName) == false) {
					if(counter != 0) {
						recipe.setImages(images);
						recipeList.add(recipe);
					}
					recipe = new Recipe();
					recipe.setRecipeName(rs.getString("recipe_name"));
					recipe.setRecipeId(rs.getInt("recipe_id"));
					images = new ArrayList<MultipartFile>();
				}
				Picture picture = new Picture();
				picture.setPicture(rs.getBytes("image"));
				picture.setPictureId(rs.getInt("image_id"));
				//pictureList.add(picture);
			
				MultipartFile image = new MockMultipartFile("tempName", rs.getString("image_name"), "image/png", rs.getBytes("image"));
				images.add(image);
				counter++;	
			}
			
			return recipeList;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}

		return null;
	}*/
	
	public List<Recipe> viewTopRecipes(ResultSet rs) {
		List<Recipe> recipeList = new ArrayList<Recipe>();
		try {
			while(rs.next()) {
				Recipe recipe = new Recipe();
				recipe.setRecipeId(rs.getInt("recipe_id"));
				recipe.setRecipeName(rs.getString("recipe_name"));
				recipeList.add(recipe);
			}
			return recipeList;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<MultipartFile> getImagesForRecipe(ResultSet resultSet) {
		try{
			List<MultipartFile> imagesList = new ArrayList<MultipartFile>();
			while(resultSet.next()) {
				MultipartFile image = new MockMultipartFile("tempFileName", resultSet.getString("image_name"), resultSet.getString("image_type"), resultSet.getBytes("image"));
				imagesList.add(image);
			}
			return imagesList;
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Tip> getTip(ResultSet resultSet) {
		recipeMapperLog.trace("INSIDE RecipeMapper - getTip() function");
		Map<String,Object> map = new HashMap<String,Object>();
		List<Tip> tipList = new ArrayList<Tip>();
		try {
			while(resultSet.next()){
				
				map.put("tip", resultSet.getString("tip"));
				
				Tip tip = new Tip(map);
				tipList.add(tip);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}


		return tipList;
	}
	
	
}
