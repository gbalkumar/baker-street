package global.coda.exception;

public class DoesntExistException extends Exception {
	String errorObject;
	
	public DoesntExistException(String errorObject) {
		this.errorObject = errorObject;
	}
	public String toString() {
		return errorObject + " doesn't exist!";
	}
}
