package global.coda.exception;

public class NoObjectsException extends Exception{
	String errorObject;
	
	public NoObjectsException(String errorObject) {
		this.errorObject = errorObject;
	}
	public String toString() {
		return "Error! There are no " + errorObject;
	}
}
