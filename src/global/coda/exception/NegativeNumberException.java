package global.coda.exception;

public class NegativeNumberException extends Exception {
	String errorObject;
	
	public NegativeNumberException(String errorObject) {
		this.errorObject = errorObject;
	}
	public String toString() {
		return "Error! You have entered " + errorObject + " as a negative number!";
	}
}
