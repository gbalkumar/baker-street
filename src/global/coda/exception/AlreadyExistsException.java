package global.coda.exception;

public class AlreadyExistsException extends Exception{
	String errorObject;
	
	public AlreadyExistsException(String errorObject) {
		this.errorObject = errorObject;
	}
	public String toString() {
		return errorObject + " already exists!";
	}
}
