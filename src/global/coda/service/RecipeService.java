package global.coda.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import global.coda.dao.RecipeDAO;
import global.coda.model.Comment;
import global.coda.model.Instruction;
import global.coda.model.Recipe;
import global.coda.model.Tip;

public interface RecipeService {
	
	public RecipeDAO getRecipeDAO();
	public void setRecipeDAO(RecipeDAO recipeDAO);
	
	public List<String> ingredientAutoComplete();
	public List<String> metricAutoComplete();
	
	public int addComment(Comment comment);
	public boolean addToFavourite (int recipeId, int userId);
	public boolean removeFromFavourite( int recipeId, int userId);

	public Recipe viewOneRecipeDetails(int recipeId, int userId);
	public Recipe viewOneRecipeDetails(int recipeId); // to use when user not logged in
	
	public int createRecipe(Recipe recipe);
	public int insertSingleInstruction(Instruction instruction, int recipeId);
	public int insertSingleTip(Tip tip, int recipeId);

	public boolean approveRecipe ( int recipeId, int userId);
	
	public boolean isUserAdmin(int userId);
	public boolean isPresentUserId(int userId);
	public boolean isPresentRecipeId (int recipeId);
	public boolean isRecipeApproved(int recipeId);
	public boolean isAlreadyFavourite (int recipeId, int userId);

	public List<Recipe> viewTopRecipes();
	public List<Recipe> viewMostPopularRecipes();
	public List<Recipe> viewBakersFavouriteRecipes();
	public List<Recipe> viewUserFavouriteRecipes(int userId);
	public List<Recipe> viewUserFavouriteRecipesAllDetails(int userId);
	
	public int incrementViewCountForRecipe(int recipeId);
	
	public List<MultipartFile> getImagesForRecipe(int recipeId);
	public int createUserRecipeRating(int recipeId, int userId, double rating);

	public List<Recipe> viewAllRecipes(int userId);
	public List<Recipe> viewAllApprovedRecipes();
	public List<Recipe> viewAllUnapprovedRecipes( int userId);
	
}
