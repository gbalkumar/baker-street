package global.coda.service;

import global.coda.model.Message;

public interface MessageService {
	public boolean send(Message message);
}
