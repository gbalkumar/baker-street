package global.coda.service;

import java.util.Map;
import global.coda.model.User;

public interface UserService {

	public int createUser(String mailId);
	public int loginUser(User user) ;
	public void updateProfile(int userId, User user);
	public boolean isUserExists(User user);
	public boolean isAlreadyRequested(User user);
	public boolean generateForgotPasswordLink(User user);
	public String isTokenValid(String token);
	public boolean updatePassword(User user);
	public void deletePasswordUpdatedTokens(User user);
	public boolean isAdmin(String userEmail);
	public User userProfile(String userEmail);
	public boolean isUserValidated(User user);
	public void generateUserValidationKey(User user);
	public String isUserTokenValid(String token);
	public void makeUserValid(String mailId);
	public void deleteVerifiedUserTokens(String mailId);
	public void deleteExpiredPassowrdTokens();
}
