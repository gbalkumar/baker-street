package global.coda.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import global.coda.dao.RecipeDAO;
import global.coda.model.Comment;
import global.coda.model.Instruction;
import global.coda.model.Recipe;
import global.coda.model.Tip;
import global.coda.service.RecipeService;

public class RecipeServiceImpl implements RecipeService {
	
	private static final Logger recipeServiceImpLog = LogManager.getLogger(RecipeServiceImpl.class.getName());

	private RecipeDAO recipeDAO;
	
	public RecipeDAO getRecipeDAO() {
		return recipeDAO;
	}
	
	public void setRecipeDAO(RecipeDAO recipeDAO) {
		this.recipeDAO = recipeDAO;
	}

	public boolean addToFavourite (int recipeId, int userId) {
		
		recipeServiceImpLog.trace("inside recipeServiceImpl addToFavourite");
		return recipeDAO.addToFavourite(recipeId, userId);	
	}
	
	public boolean removeFromFavourite(int recipeId, int userId) {
		recipeServiceImpLog.trace("inside recipeServiceImpl removeFromFavourite");
		return recipeDAO.removeFromFavourite(recipeId, userId);
	}
	
	public List<String> ingredientAutoComplete() {
		
		recipeServiceImpLog.trace("inside recipeServiceImpl ingredientAutoComplete");
		return recipeDAO.IngredientAutoComplete();
	}
	
	public List<String> metricAutoComplete(){
		
		recipeServiceImpLog.trace("inside recipeServiceImpl metricAutoComplete");
		return recipeDAO.MetricAutoComplete();
		
	}
	
	
	public int addComment(Comment comment) {
		
		recipeServiceImpLog.trace("inside RecipeService- AddComment() function");
		//Comment comment = new Comment(map);
		int commentId = recipeDAO.addComment(comment);
		recipeServiceImpLog.trace("inside RecipeService- CommentID: " + commentId);
		return commentId;
	}
	
	public Recipe viewOneRecipeDetails(int recipeId, int userId){
		
		recipeServiceImpLog.trace("inside recipeServiceImpl viewOneRecipeDetails");
		Recipe recipe = new Recipe();
		recipe = recipeDAO.viewOneRecipeDetails(recipeId, userId);
		return recipe;
		
	}
	
	public Recipe viewOneRecipeDetails(int recipeId){
		
		recipeServiceImpLog.trace("inside recipeServiceImpl viewOneRecipeDetails");
		Recipe recipe = new Recipe();
		recipe = recipeDAO.viewOneRecipeDetails(recipeId);
		return recipe;
		
	}

	@Override
	public int createRecipe(Recipe recipe) {
		
		recipeServiceImpLog.trace("inside recipeServiceImpl createRecipe");
		int recipeId = 0;
	
		if(recipeDAO == null) {
			//System.out.println("\n\tRecipe DAO is null!");
		}
		try {
			recipeId = recipeDAO.createRecipe(recipe);
			recipeServiceImpLog.trace("Service RecipeId: "+recipeId);
		
		} catch (SQLException e) {
			recipeServiceImpLog.trace("In serviec createRecipe() catch Block- ERROR: " + e);
		}
		return recipeId;
	}
	
	public boolean approveRecipe ( int recipeId, int userId){
		
		recipeServiceImpLog.trace("inside recipeServiceImpl approveRecipe");

		boolean approveStatus = false;
		approveStatus = recipeDAO.approveRecipe(recipeId, userId);
		return approveStatus;
	
	}
	
	/*public void insertSingleInstruction(Map<String, Object> map){
		
		recipeServiceImpLog.trace("inside recipeServiceImpl insertSingleInstruction");
		Instruction instruction = new Instruction(map);
		recipeDAO.insertSingleInstruction(instruction);
	}*/
	
	public int insertSingleInstruction(Instruction instruction, int recipeId) {
		recipeServiceImpLog.trace("inside recipeServiceImpl insertSingleInstruction");
		return recipeDAO.insertSingleInstruction(instruction, recipeId);
	}


	@Override
	public List<Recipe> viewTopRecipes() {
		return recipeDAO.viewTopRecipes();
	}
	
	public List<MultipartFile> getImagesForRecipe(int recipeId) {
		return recipeDAO.getImagesForRecipe(recipeId);
	}
	
	public boolean isPresentUserId( int userId){
		
		recipeServiceImpLog.trace("inside recipeServiceImpl isPresentUserId");
		return recipeDAO.isPresentUserId(userId);
		
	}
	public boolean isPresentRecipeId (int recipeId) {
		
		recipeServiceImpLog.trace("inside recipeServiceImpl isPresentRecipeId");
		return recipeDAO.isPresentRecipeId(recipeId);
	}

	public boolean isRecipeApproved(int recipeId){
		
		recipeServiceImpLog.trace("inside recipeServiceImpl isApprovedRecipe");
		return recipeDAO.isRecipeApproved(recipeId);
		
	}

	public boolean isAlreadyFavourite (int recipeId, int userId){
		recipeServiceImpLog.trace("inside recipeServiceImpl isAreadyFavourite");
		return recipeDAO.isAlreadyFavourite(recipeId, userId);

	}

	public int createUserRecipeRating(int recipeId, int userId, double rating) {
		return recipeDAO.createUserRecipeRating(recipeId, userId, rating);
	}


	@Override
	public List<Recipe> viewMostPopularRecipes() {
		return recipeDAO.viewMostPopularRecipes();
	}


	@Override
	public List<Recipe> viewBakersFavouriteRecipes() {
		return recipeDAO.viewBakersFavouriteRecipes();
	}


	@Override
	public List<Recipe> viewUserFavouriteRecipes(int userId) {
		return recipeDAO.viewUserFavouriteRecipes(userId);
	}

	

	@Override
	public int incrementViewCountForRecipe(int recipeId) {
		return recipeDAO.incrementViewCountForRecipe(recipeId);
	}
	
	public List<Recipe> viewAllRecipes(int userId) {
		return recipeDAO.viewAllRecipes(userId);
	}
	public List<Recipe> viewAllApprovedRecipes() {
		return recipeDAO.viewAllApprovedRecipes();
	}
	
	public List<Recipe> viewAllUnapprovedRecipes( int userId){
		return recipeDAO.viewAllUnapprovedRecipes(userId);
	}

	@Override
	public boolean isUserAdmin(int userId) {
		return recipeDAO.isUserAdmin(userId);
	}

	@Override
	public List<Recipe> viewUserFavouriteRecipesAllDetails(int userId) {
		return recipeDAO.viewUserFavouriteRecipesAllDetails(userId);
	}

	@Override
	public int insertSingleTip(Tip tip, int recipeId) {
		return recipeDAO.insertSingleTip(tip, recipeId);
	}

	
}
