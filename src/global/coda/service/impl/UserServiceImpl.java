package global.coda.service.impl;

import java.io.IOException;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

import global.coda.dao.UserDAO;
import global.coda.dao.impl.RecipeDAOImpl;
import global.coda.model.User;
import global.coda.service.UserService;
import global.coda.util.EmailTemplateHtmlUtil;

public class UserServiceImpl implements UserService {
	private static final Logger userServiceImpLog = LogManager.getLogger(RecipeDAOImpl.class.getName());

	UserDAO userDAO;

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public int createUser(String mailId) {
		
		userServiceImpLog.trace("Inside user Service IMpl createUSer method()");

		int userId = userDAO.createUser(mailId);
		userServiceImpLog.trace("inside UserService- UserID: "+userId);
		return userId;
	}
	public boolean isUserExists(User user) {
		
		return userDAO.isUserExists(user);
		
	}
	
	public int loginUser(User user) {
		
		userServiceImpLog.trace("Inside user Service IMpl login User method()");
				
		int userId= userDAO.loginUser(user);
		userServiceImpLog.trace("inside UserServiceImpl- loginUser : "+userId);
		return userId;
	}
	
	public void updateProfile(int userId,User user) {

		boolean isUpdateSuccessful = userDAO.updateProfile(userId, user);
		userServiceImpLog.trace("inside UserServiceImpl- updateUser : "+isUpdateSuccessful);
			
	}


	public boolean isAlreadyRequested(User user) {
		
		return userDAO.isAlreadyRequested(user);
	}
	
	
	
	public boolean generateForgotPasswordLink(User user) {
		try {
		
		String tokenId =UUID.randomUUID().toString();	
		Email from = new Email("noreply@baker-street.com");
	    String subject = "Baker-Street Change Password request";

	    Email to = new Email(user.getUserEmail());
	    String hostName=  System.getProperty("APP_HOSTNAME");
	    String messageBody="Warm Wishes from Baker-Street. Your request for changing password is received and responded.Please click on the button below to change the password. Please note That the link Expires in 30 Minutes(Please Do not forward this mail to anyone)";
	    String body = EmailTemplateHtmlUtil.HEADER+messageBody+EmailTemplateHtmlUtil.BUTTON+ hostName+"/change-password?token="+tokenId+EmailTemplateHtmlUtil.AFTERBUTTONLINK+"Change Password"+EmailTemplateHtmlUtil.FOOTER;
		userServiceImpLog.trace("inside UserServiceImpl- generateForgot Password \n: "+body);

	    Content content = new Content("text/html",body);

	    Mail mail = new Mail(from, subject, to, content);

	    
		    String apiKey =   System.getProperty("SENDGRID_API_KEY");
			userServiceImpLog.trace("inside UserServiceImpl- generateForgot Password sendgrid api key"+apiKey);

		    SendGrid sg = new SendGrid(apiKey);
		
		    Request request = new Request();
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());

	      Response response = sg.api(request);
			userServiceImpLog.trace(response.getStatusCode());
	     
			 return userDAO.generateForgotPasswordLink(user,tokenId);
		   
	    } catch (IOException ex) {
	      	ex.printStackTrace();
	      	return false;
	    }
		
	}
	
	public String isTokenValid(String token) {
		
		String mailId=userDAO.isTokenValid(token);
		userServiceImpLog.trace("mail Id"+mailId);
		userDAO.deleteExpiredTokens();
		if(mailId!=null) {
			return mailId;	
		}
		return null;
	}

	
	public boolean updatePassword(User user) {
		
		return userDAO.updatePassword(user);
	}

	
	public void deletePasswordUpdatedTokens(User user) {
		
		userDAO.deletePasswordUpdatedTokens(user);
	}

	
	public boolean isAdmin(String userEmail) {
		
		return userDAO.isAdmin(userEmail);
	}
	

	public User userProfile(String userEmail) {

		User user = new User();
		
		user=userDAO.userProfile(userEmail);

		return user;
	}


	public boolean isUserValidated(User user) {
	
		return userDAO.isUserValidated(user);		

	}

	public void generateUserValidationKey(User user) {
		
		 try {
		String validationKey =UUID.randomUUID().toString();
		Email from = new Email("noreply@baker-street.com");

	    String subject = "Baker-Street Account Verification";

	    Email to = new Email(user.getUserEmail());
	    String messageBody="We are very happy to welcome you to Baker-Street, Please verify your account by clicking the button provided below and enjoy the World of Cooking.";
	    String body = EmailTemplateHtmlUtil.HEADER+messageBody+EmailTemplateHtmlUtil.BUTTON +  System.getProperty("APP_HOSTNAME")+"/account-verify?token="+validationKey+EmailTemplateHtmlUtil.AFTERBUTTONLINK+"Verify Account"+EmailTemplateHtmlUtil.FOOTER;
		userServiceImpLog.trace("inside UserServiceImpl- generateForgot Password \n: "+body);

	    Content content = new Content("text/html",body);

	    Mail mail = new Mail(from, subject, to, content);
    
	    SendGrid sg = new SendGrid(  System.getProperty("SENDGRID_API_KEY"));
		
	    Request request = new Request();
	   
	    	
	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
	      Response response = sg.api(request);
			userServiceImpLog.trace(response.getStatusCode());
	     
			userDAO.generateUserValidationKey(user,validationKey);

	    } catch (IOException ex) {
	      	ex.printStackTrace();
	    }
		
	}

	public String isUserTokenValid(String token) {
		
		String mailId=userDAO.isUserTokenValid(token);
		userServiceImpLog.trace("mail Id"+mailId);
		userDAO.deleteExpiredUserTokens();
		if(mailId!=null) {
			return mailId;	
		}
		
		
		
		return null;
	}

	public void makeUserValid(String mailId) {
		
		
		
	}

	public void deleteVerifiedUserTokens(String mailId) {
			
		userDAO.deleteVerifiedUserTokens(mailId);
	}

	public void deleteExpiredPassowrdTokens() {
		userDAO.deleteExpiredTokens();
		
	}
	
	
	

}
