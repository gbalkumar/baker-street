package global.coda.service.impl;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

import global.coda.model.Message;
import global.coda.service.MessageService;

public class MessageServiceImpl implements MessageService{

	private static final Logger messageServiceImplLog = LogManager.getLogger(MessageServiceImpl.class.getName());

	public boolean send(Message message) {
		messageServiceImplLog.trace("inside service impl send func()");
		
		Email from = new Email(message.getFromEmail());
	    String subject = message.getSubject();
		messageServiceImplLog.trace("send func() subject"+subject);

	    Email to = new Email(message.getToEmail());
		messageServiceImplLog.trace("send func() to"+to+"email"+message.getToEmail());

	    Content content = new Content("text/html", message.getBody());
	    
		messageServiceImplLog.trace("send func() content"+content+"body"+message.getBody());

	    Mail mail = new Mail(from, subject, to, content);

	    try {

		    
		    SendGrid sg = new SendGrid(  System.getProperty("SENDGRID_API_KEY"));

		    Request request = new Request();
	    	messageServiceImplLog.trace("inside try of service impl send func()");

	      request.setMethod(Method.POST);
	      request.setEndpoint("mail/send");
	      request.setBody(mail.build());
			messageServiceImplLog.trace("method and endpoint and setBody successful");

	      Response response = sg.api(request);
	      //System.out.println(response.getStatusCode());
	      //System.out.println(response.getBody());
	      //System.out.println(response.getHeaders());
	      return true;
	      
	    } catch (IOException ex) {
	      	ex.printStackTrace();
	      	return false;
	    }
	  
	}
	
	
	
	
	
	
	
	
	
	
}
