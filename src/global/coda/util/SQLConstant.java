package global.coda.util;

public class SQLConstant {

	
	public static final String AUTOCOMPLETE_INGREDIENT_NAME = "select ingredient_name from ingredient";
	public static final String AUTOCOMPLETE_METRIC_NAME = "select metric_name from metric";

	public static final String INSERT_COMMENT = "insert into comment(recipe_id, user_id, comment) values (?, ?, ?)";

	//public static final String GET_STYLE_NAME = "select style_id from style where style_id = ?";
	public static final String ADMIN_INSERT_RECIPE = "insert into recipe (submitter_id, recipe_name, heading, preparation_time, cook_time, readyin_time, is_approved) values (?, ?, ?, ?, ?, ?, ?)";

	public static final String INSERT_RECIPE = "insert into recipe (submitter_id, recipe_name, heading, preparation_time, cook_time, readyin_time) values (?, ?, ?, ?, ?, ?)";
	public static final String SELECT_STYLE_ID_WITH_NAME = "select * from style where UPPER(style_name) = UPPER(?)";
	public static final String SELECT_STYLE_NAME_WITH_ID = "select style_name from style where style_id = ?";
	
	public static final String SELECT_INGREDIENT_ID_WITH_NAME = "select * from INGREDIENT where UPPER(INGREDIENT_NAME) = UPPER(?)";	
	public static final String SELECT_METRIC_ID_WITH_NAME = "select * from METRIC where UPPER(METRIC_NAME) = UPPER(?)";

	public static final String INSERT_METRIC_NAME = "insert into metric (metric_name) values (?)";
	public static final String INSERT_INGREDIENT_NAME = "INSERT INTO INGREDIENT (INGREDIENT_NAME) VALUES (?)";
	public static final String INSERT_IN_RECIPE_INGREDIENT_REL = "INSERT INTO RECIPE_INGREDIENT_REL(recipe_id, ingredient_id, quantity, metric_id) values (?, ?, ?, ?)";

	public static final String ADD_RECIPE_TO_USER_FAVOURITE = "insert into user_favourite_recipe values (?, ?)";
	public static final String REMOVE_RECIPE_FROM_USER_FAVOURITE = "DELETE FROM USER_FAVOURITE_RECIPE WHERE USER_ID = ? AND RECIPE_ID = ?";

	public static final String SELECT_ISALREADY_FAVOURITE = "select * from user_favourite_recipe where user_id = ? and recipe_id = ?";
	public static final String INSERT_STYLE_NAME = "insert into style (style_name) values (?)";
	public static final String UPDATE_STYLE_ID_IN_RECIPE = "update recipe set style_id = ? where recipe_id = ?";
	public static final String APPROVE_RECIPE = "update recipe set is_approved = 't' where recipe_id = ?";
	public static final String INSERT_SINGLE_INSTRUCTION = "insert into instruction(recipe_id, step, instruction) values (?, ?, ?)";
	public static final String INSERT_SINGLE_TIP = "INSERT INTO TIP(RECIPE_ID, TIP) VALUES(?, ?)";

	
	
	public static final String SELECT_USER_WITH_ID = "select * from USER_ACCOUNT where USER_ID = ?";
	public static final String SELECT_RECIPE_WITH_ID = "select * from RECIPE where RECIPE_ID = ?";
	public static final String SELECT_ISADMIN = "select is_admin from user_account where user_id = ?";
	public static final String SELECT_ISAPPROVED = "select is_approved from recipe where recipe_id = ?";
//	public static final String SELECT_ALL_APPROVED_RECIPES = "SELECT * FROM RECIPE WHERE IS_APPROVED = 't'";
	
	public static final String SELECT_ALL_UNAPPROVED_RECIPE = "SELECT R.IS_APPROVED, R.RECIPE_ID, R.RECIPE_NAME, S.STYLE_NAME, U.USER_EMAIL, R.VIEW_COUNT, R.AVERAGE_RATING  FROM RECIPE R JOIN USER_ACCOUNT U ON (R.SUBMITTER_ID = U.USER_ID) JOIN STYLE S ON (R.STYLE_ID = S.STYLE_ID)   WHERE IS_APPROVED = FALSE";
	
	public static final String SELECT_ALL_APPROVED_RECIPES = "SELECT R.IS_APPROVED, R.RECIPE_ID, R.RECIPE_NAME, S.STYLE_NAME, U.USER_EMAIL, R.VIEW_COUNT, R.AVERAGE_RATING  FROM RECIPE R JOIN USER_ACCOUNT U ON (R.SUBMITTER_ID = U.USER_ID) JOIN STYLE S ON (R.STYLE_ID = S.STYLE_ID)   WHERE IS_APPROVED = TRUE";
	public static final String SELECT_ALL_RECIPES = "SELECT R.IS_APPROVED, R.RECIPE_ID, R.RECIPE_NAME, S.STYLE_NAME, U.USER_EMAIL, R.VIEW_COUNT, R.AVERAGE_RATING  FROM RECIPE R JOIN USER_ACCOUNT U ON (R.SUBMITTER_ID = U.USER_ID) JOIN STYLE S ON (R.STYLE_ID = S.STYLE_ID)";

	public static final String SELECT_SINGLE_RECIPE_ADMIN = "SELECT * FROM RECIPE WHERE recipe_id = ?";

	public static final String SELECT_SINGLE_RECIPE = "SELECT * FROM RECIPE WHERE IS_APPROVED = 't'and recipe_id = ?";
	public static final String SELECT_INSTRUCTION_FOR_RECIPE = "SELECT * FROM INSTRUCTION WHERE RECIPE_ID = ? order by instruction_id";
	public static final String SELECT_TIP_FOR_RECIPE = "SELECT * FROM TIP WHERE RECIPE_ID = ? ORDER BY TIP_ID";

	
	public static final String SELECT_INGREDIENT_FOR_RECIPE_ID = "select ri.quantity ,mt.metric_name,i.ingredient_name "
			+ "from ingredient i join recipe_ingredient_rel ri on (i.ingredient_id = ri.ingredient_id) "
			+ "join metric mt on (mt.metric_id = ri.metric_id) where ri.recipe_id = ?";
	public static final String SELECT_COMMENT_FOR_RECIPE_ID = "select u.user_email, co.comment ,co.comment_submitted_time,co.comment_id from comment co "
			+ "join user_account u on (u.user_id = co.user_id) where co.recipe_id = ?";

	public static final String UPDATE_PASSWORD = "UPDATE USER_ACCOUNT SET USER_PASSWORD=CRYPT(?,gen_salt('md5')) where UPPER(user_email)=UPPER(?)";

	public static final String GET_COMMENT_YEAR_DAYS_HOUR_MINS_SEC = "select extract(year from timezone('utc'::text, now())-comment_submitted_time), extract(day from timezone('utc'::text, now())-comment_submitted_time), extract(hour from timezone('utc'::text, now())-comment_submitted_time), extract(minute from timezone('utc'::text, now())-comment_submitted_time), extract(second from timezone('utc'::text, now())-comment_submitted_time) FROM comment WHERE recipe_id = ? and comment_id = ?";

	public static final String CREATE_USER = "INSERT INTO USER_ACCOUNT (USER_EMAIL,USER_PASSWORD) SELECT USER_EMAIL,PASSWORD FROM USER_VALIDATION WHERE USER_EMAIL=?";
	public static final String IS_USER_EMAIL_EXISTS = "SELECT * FORM USER_ACCOUNT WHERE UPPER(USER_EMAIL) = UPPER(?)";
	public static final String LOGIN_USER = "SELECT * FROM USER_ACCOUNT WHERE UPPER(USER_EMAIL)=UPPER(?) AND USER_PASSWORD=(CRYPT(?,USER_PASSWORD))";
	public static final String UPDATE_PROFILE = "UPDATE USER_ACCOUNT SET USER_NAME=?,USER_ADDRESS=?,USER_PHONE_NUMBER=?,USER_WEBSITE=? WHERE USER_ID=?";
	public static final String IS_USER_EXISTS="SELECT * FROM USER_ACCOUNT WHERE UPPER(USER_EMAIL)=UPPER(?)";
	public static final String IS_LINK_ALREADY_REQUESTED = "SELECT * FROM FORGOT_PASSWORD WHERE UPPER(USER_EMAIL)=UPPER(?)";
	public static final String GENERATE_FORGOT_PASSWORD_LINK = "INSERT INTO FORGOT_PASSWORD (USER_EMAIL,FORGOT_ID) VALUES(?,?)";
	public static final String IS_TOKEN_EXISTS = "SELECT USER_EMAIL FROM FORGOT_PASSWORD WHERE FORGOT_ID = ? AND (timezone('utc'::text, now())-LINK_CREATED_TIME)<'00:30:00.0'";
	public static final String DELETE_EXPIRED_TOKENS = "DELETE FROM FORGOT_PASSWORD WHERE (timezone('utc'::text, now())-LINK_CREATED_TIME)>'00:30:00.0'";
	public static final String SELECT_RECIPE = "SELECT * FROM RECIPE";
	//public static final String SELECT_TOP_RECIPES = "SELECT RECIPE_NAME, IMAGE FROM RECIPE JOIN IMAGE ON (RECIPE.RECIPE_ID = IMAGE.RECIPE_ID) ORDER BY VIEW_COUNT DESC LIMIT ?";
	
	//public static final String SELECT_TOP_RECIPES = "SELECT RECIPE.RECIPE_ID, RECIPE_NAME, IMAGE_ID, IMAGE, IMAGE_NAME FROM RECIPE JOIN IMAGE ON (RECIPE.RECIPE_ID = IMAGE.RECIPE_ID) WHERE RECIPE.RECIPE_ID IN (SELECT RECIPE_ID FROM RECIPE ORDER BY VIEW_COUNT DESC LIMIT ?)";
	//public static final String SELECT_TOP_RECIPES = "with summary as ( select r.recipe_name, i.image, ROW_NUMBER() OVER(PARTITION BY r.recipe_name ORDER BY r.views) as rk from recipe r, image i where r.recipe_id = i.recipe_id) select s.* from summary s where s.rk = 1 limit ?";
	public static final String SELECT_TOP_RECIPES = "SELECT RECIPE_ID, RECIPE_NAME FROM RECIPE WHERE IS_APPROVED = true ORDER BY VIEW_COUNT DESC LIMIT ?";
	public static final String SELECT_MOST_POPULAR_RECIPES = "SELECT RECIPE_ID, RECIPE_NAME FROM RECIPE WHERE IS_APPROVED = true ORDER BY AVERAGE_RATING DESC LIMIT ?";
	public static final String SELECT_USER_FAVOURITE_RECIPES = "SELECT RECIPE_ID, RECIPE_NAME FROM RECIPE WHERE IS_APPROVED = true AND RECIPE_ID IN (SELECT RECIPE_ID FROM USER_FAVOURITE_RECIPE WHERE USER_ID = ?)";
	public static final String SELECT_BAKER_FAVOURITE_RECIPES = "SELECT RECIPE_ID, RECIPE_NAME FROM RECIPE WHERE IS_APPROVED = true AND RECIPE_ID IN (SELECT RECIPE_ID FROM USER_FAVOURITE_RECIPE WHERE USER_ID IN (SELECT USER_ID FROM USER_ACCOUNT WHERE IS_ADMIN = true))";

	public static final String SELECT_ALL_APPROVED_USER_FAVOURITE_RECIPES = "SELECT R.IS_APPROVED, R.RECIPE_ID, R.RECIPE_NAME, S.STYLE_NAME, U.USER_EMAIL, R.VIEW_COUNT, R.AVERAGE_RATING  FROM RECIPE R JOIN USER_ACCOUNT U ON (R.SUBMITTER_ID = U.USER_ID) JOIN STYLE S ON (R.STYLE_ID = S.STYLE_ID)   WHERE IS_APPROVED = TRUE AND R.RECIPE_ID IN (SELECT RECIPE_ID FROM USER_FAVOURITE_RECIPE WHERE USER_ID = ?)";
	
	public static final String INSERT_IMAGE = "INSERT INTO IMAGE(RECIPE_ID, IMAGE, IMAGE_NAME, IMAGE_TYPE) VALUES(?,?,?,?)";
	public static final String SELECT_IMAGES_FOR_RECIPE_ID = "SELECT IMAGE_ID, IMAGE, IMAGE_NAME, IMAGE_TYPE FROM IMAGE WHERE RECIPE_ID = ?";

	public static final String DELETE_PASSWORD_UPDATED_TOKENS = "DELETE FROM FORGOT_PASSWORD WHERE UPPER(USER_EMAIL) = UPPER(?)";
	public static final String INSERT_USER_RECIPE_RATING = "INSERT INTO RECIPE_USER_RATING(RATING, RECIPE_ID, USER_ID) VALUES(?, ?, ?)";
	public static final String UPDATE_USER_RECIPE_RATING = "UPDATE RECIPE_USER_RATING SET RATING = ? WHERE RECIPE_ID = ? AND USER_ID = ?";

	public static final String SELECT_USER_RATING_WITH_USER_ID_RECIPE_ID = "SELECT * FROM RECIPE_USER_RATING WHERE USER_ID = ? AND RECIPE_ID = ?";
	public static final String SELECT_AVERAGE_RATING_FOR_RECIPE_ID = "SELECT AVG(RATING) FROM RECIPE_USER_RATING GROUP BY RECIPE_ID HAVING RECIPE_ID = ?";
	public static final String UPDATE_AVERAGE_RATING_FOR_RECIPE_ID = "UPDATE RECIPE SET AVERAGE_RATING = ? WHERE RECIPE_ID = ?";

	public static final String INCREMENT_VIEW_COUNT_FOR_RECIPE_ID = "UPDATE RECIPE SET VIEW_COUNT = VIEW_COUNT + 1 WHERE RECIPE_ID = ? and IS_APPROVED = true";
	public static final String IS_ADMIN = "SELECT IS_ADMIN FROM USER_ACCOUNT WHERE UPPER(USER_EMAIL)=UPPER(?)";
	public static final String SELECT_USER_PROFILE = "SELECT USER_NAME,USER_ADDRESS,USER_PHONE_NUMBER,USER_WEBSITE FROM USER_ACCOUNT WHERE UPPER(USER_EMAIL)=UPPER(?)";
	public static final String IS_USER_IN_VALIDATION_QUEUE = "SELECT ID FROM USER_VALIDATION WHERE UPPER(USER_EMAIL)=UPPER(?)";
	public static final String GENERATE_USER_VALIDATION_KEY = "INSERT INTO USER_VALIDATION(USER_EMAIL,PASSWORD,USER_VALIDATION_KEY) VALUES(?, CRYPT(?,gen_salt('md5')) ,?)";
	public static final String IS_USER_TOKEN_EXISTS = "SELECT USER_EMAIL FROM USER_VALIDATION WHERE USER_VALIDATION_KEY = ? AND (EXTRACT(DAY FROM timezone('utc'::text, now())-KEY_CREATED_TIME))<8";
	public static final String DELETE_USER_VERIFIED_TOKENS = "DELETE FROM USER_VALIDATION WHERE UPPER(USER_EMAIL)=UPPER(?)";
	public static final String DELETE_EXPIRED_USER_TOKENS="DELETE FROM USER_VALIDATION WHERE (EXTRACT(DAY FROM timezone('utc'::text, now())-KEY_CREATED_TIME))<8";
}
