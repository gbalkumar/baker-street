package global.coda.util;

import java.util.Date;
import java.sql.Timestamp;

public class InputUtil {
	
	public static Timestamp getCurrentTimestamp() {
		
		//link ref: https://stackoverflow.com/questions/23068676/how-to-get-current-timestamp-in-string-format-in-java-yyyy-mm-dd-hh-mm-ss
		//String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(System.currentTimeMillis()));		
		//return timestamp;
		//this returns String object we need Timestamp object so use below code....
		
		Date date = new Date();
		Timestamp currentTime = new java.sql.Timestamp(date.getTime());
	   
	    return currentTime;
		
	}


}
