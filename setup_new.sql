--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: comment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE comment (
    comment_id integer NOT NULL,
    recipe_id integer,
    user_id integer,
    comment_submitted_time timestamp without time zone DEFAULT timezone('utc'::text, now()),
    comment character varying(10000) NOT NULL
);


--
-- Name: comment_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE comment_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comment_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE comment_comment_id_seq OWNED BY comment.comment_id;


--
-- Name: comment_tester; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE comment_tester (
    comment_id integer NOT NULL,
    recipe_id integer,
    user_id integer,
    comment_submitted_time timestamp without time zone DEFAULT timezone('utc'::text, now()),
    comment character varying(10000) NOT NULL
);


--
-- Name: comment_tester_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE comment_tester_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comment_tester_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE comment_tester_comment_id_seq OWNED BY comment_tester.comment_id;


--
-- Name: image; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE image (
    image_id integer NOT NULL,
    recipe_id integer,
    image bytea,
    image_name character varying(300),
    image_type character varying(300)
);


--
-- Name: image_image_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE image_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: image_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE image_image_id_seq OWNED BY image.image_id;


--
-- Name: ingredient; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ingredient (
    ingredient_id integer NOT NULL,
    ingredient_name character varying(200) NOT NULL
);


--
-- Name: ingredient_ingredient_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ingredient_ingredient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ingredient_ingredient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ingredient_ingredient_id_seq OWNED BY ingredient.ingredient_id;


--
-- Name: instruction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE instruction (
    instruction_id integer NOT NULL,
    recipe_id integer,
    step integer NOT NULL,
    instruction character varying(400) NOT NULL,
    CONSTRAINT instruction_step_check CHECK ((step >= 0))
);


--
-- Name: instruction_instruction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE instruction_instruction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: instruction_instruction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE instruction_instruction_id_seq OWNED BY instruction.instruction_id;


--
-- Name: metric; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE metric (
    metric_id integer NOT NULL,
    metric_name character varying(100) NOT NULL
);


--
-- Name: metric_metric_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE metric_metric_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: metric_metric_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE metric_metric_id_seq OWNED BY metric.metric_id;


--
-- Name: recipe; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE recipe (
    recipe_id integer NOT NULL,
    submitter_id integer,
    recipe_name character varying(300) NOT NULL,
    style_id integer,
    recipe_submitted_time timestamp without time zone DEFAULT timezone('utc'::text, now()),
    is_approved boolean DEFAULT false NOT NULL,
    heading character varying(300) NOT NULL,
    preparation_time integer NOT NULL,
    cook_time integer NOT NULL,
    readyin_time integer NOT NULL,
    view_count integer DEFAULT 0 NOT NULL,
    average_rating numeric DEFAULT 0.0,
    CONSTRAINT recipe_cook_time_check CHECK ((cook_time >= 0)),
    CONSTRAINT recipe_preparation_time_check CHECK ((preparation_time >= 0)),
    CONSTRAINT recipe_readyin_time_check CHECK ((readyin_time >= 0))
);


--
-- Name: recipe_ingredient_rel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE recipe_ingredient_rel (
    recipe_ingredient_id integer NOT NULL,
    recipe_id integer,
    ingredient_id integer,
    quantity integer NOT NULL,
    metric_id integer,
    CONSTRAINT recipe_ingredient_rel_quantity_check CHECK ((quantity >= 0))
);


--
-- Name: recipe_ingredient_rel_recipe_ingredient_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE recipe_ingredient_rel_recipe_ingredient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recipe_ingredient_rel_recipe_ingredient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE recipe_ingredient_rel_recipe_ingredient_id_seq OWNED BY recipe_ingredient_rel.recipe_ingredient_id;


--
-- Name: recipe_recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE recipe_recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: recipe_recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE recipe_recipe_id_seq OWNED BY recipe.recipe_id;


--
-- Name: recipe_user_rating; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE recipe_user_rating (
    user_id integer NOT NULL,
    recipe_id integer NOT NULL,
    rating integer NOT NULL,
    CONSTRAINT recipe_user_rating_rating_check CHECK (((rating >= 0) AND (rating <= 5)))
);


--
-- Name: style; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE style (
    style_id integer NOT NULL,
    style_name character varying(200)
);


--
-- Name: style_style_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE style_style_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: style_style_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE style_style_id_seq OWNED BY style.style_id;


--
-- Name: testtable1; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE testtable1 (
    id integer NOT NULL,
    name character varying(40),
    views integer DEFAULT 0
);


--
-- Name: testtable1_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE testtable1_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: testtable1_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE testtable1_id_seq OWNED BY testtable1.id;


--
-- Name: testtable2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE testtable2 (
    imageid integer NOT NULL,
    rid integer,
    imagename character varying(100)
);


--
-- Name: testtable2_imageid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE testtable2_imageid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: testtable2_imageid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE testtable2_imageid_seq OWNED BY testtable2.imageid;


--
-- Name: user_account; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_account (
    user_id integer NOT NULL,
    user_address character varying(300),
    user_phone_number character varying(20),
    user_website character varying(200),
    user_password character varying(300) NOT NULL,
    is_admin boolean DEFAULT false NOT NULL,
    user_email character varying(100) NOT NULL,
    user_name character varying(100)
);


--
-- Name: user_account_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_account_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_account_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_account_user_id_seq OWNED BY user_account.user_id;


--
-- Name: user_favourite_recipe; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_favourite_recipe (
    user_id integer NOT NULL,
    recipe_id integer NOT NULL
);


--
-- Name: comment comment_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY comment ALTER COLUMN comment_id SET DEFAULT nextval('comment_comment_id_seq'::regclass);


--
-- Name: comment_tester comment_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY comment_tester ALTER COLUMN comment_id SET DEFAULT nextval('comment_tester_comment_id_seq'::regclass);


--
-- Name: image image_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY image ALTER COLUMN image_id SET DEFAULT nextval('image_image_id_seq'::regclass);


--
-- Name: ingredient ingredient_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ingredient ALTER COLUMN ingredient_id SET DEFAULT nextval('ingredient_ingredient_id_seq'::regclass);


--
-- Name: instruction instruction_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY instruction ALTER COLUMN instruction_id SET DEFAULT nextval('instruction_instruction_id_seq'::regclass);


--
-- Name: metric metric_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY metric ALTER COLUMN metric_id SET DEFAULT nextval('metric_metric_id_seq'::regclass);


--
-- Name: recipe recipe_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe ALTER COLUMN recipe_id SET DEFAULT nextval('recipe_recipe_id_seq'::regclass);


--
-- Name: recipe_ingredient_rel recipe_ingredient_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_ingredient_rel ALTER COLUMN recipe_ingredient_id SET DEFAULT nextval('recipe_ingredient_rel_recipe_ingredient_id_seq'::regclass);


--
-- Name: style style_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY style ALTER COLUMN style_id SET DEFAULT nextval('style_style_id_seq'::regclass);


--
-- Name: testtable1 id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY testtable1 ALTER COLUMN id SET DEFAULT nextval('testtable1_id_seq'::regclass);


--
-- Name: testtable2 imageid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY testtable2 ALTER COLUMN imageid SET DEFAULT nextval('testtable2_imageid_seq'::regclass);


--
-- Name: user_account user_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_account ALTER COLUMN user_id SET DEFAULT nextval('user_account_user_id_seq'::regclass);


--
-- Name: comment comment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (comment_id);


--
-- Name: comment_tester comment_tester_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comment_tester
    ADD CONSTRAINT comment_tester_pkey PRIMARY KEY (comment_id);


--
-- Name: image image_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_pkey PRIMARY KEY (image_id);


--
-- Name: ingredient ingredient_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (ingredient_id);


--
-- Name: instruction instruction_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY instruction
    ADD CONSTRAINT instruction_pkey PRIMARY KEY (instruction_id);


--
-- Name: metric metric_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY metric
    ADD CONSTRAINT metric_pkey PRIMARY KEY (metric_id);


--
-- Name: recipe_ingredient_rel recipe_ingredient_rel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_ingredient_rel
    ADD CONSTRAINT recipe_ingredient_rel_pkey PRIMARY KEY (recipe_ingredient_id);


--
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (recipe_id);


--
-- Name: recipe_user_rating recipe_user_rating_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_user_rating
    ADD CONSTRAINT recipe_user_rating_pkey PRIMARY KEY (user_id, recipe_id);


--
-- Name: style style_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY style
    ADD CONSTRAINT style_pkey PRIMARY KEY (style_id);


--
-- Name: testtable1 testtable1_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY testtable1
    ADD CONSTRAINT testtable1_pkey PRIMARY KEY (id);


--
-- Name: testtable2 testtable2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY testtable2
    ADD CONSTRAINT testtable2_pkey PRIMARY KEY (imageid);


--
-- Name: user_account user_account_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_account
    ADD CONSTRAINT user_account_pkey PRIMARY KEY (user_id);


--
-- Name: user_account user_account_user_email_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_account
    ADD CONSTRAINT user_account_user_email_key UNIQUE (user_email);


--
-- Name: user_favourite_recipe user_favourite_recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_favourite_recipe
    ADD CONSTRAINT user_favourite_recipe_pkey PRIMARY KEY (user_id, recipe_id);


--
-- Name: comment comment_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id);


--
-- Name: comment comment_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_user_id_fkey FOREIGN KEY (user_id) REFERENCES user_account(user_id);


--
-- Name: image image_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY image
    ADD CONSTRAINT image_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id);


--
-- Name: instruction instruction_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY instruction
    ADD CONSTRAINT instruction_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id);


--
-- Name: recipe_ingredient_rel recipe_ingredient_rel_ingredient_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_ingredient_rel
    ADD CONSTRAINT recipe_ingredient_rel_ingredient_id_fkey FOREIGN KEY (ingredient_id) REFERENCES ingredient(ingredient_id);


--
-- Name: recipe_ingredient_rel recipe_ingredient_rel_metric_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_ingredient_rel
    ADD CONSTRAINT recipe_ingredient_rel_metric_id_fkey FOREIGN KEY (metric_id) REFERENCES metric(metric_id);


--
-- Name: recipe_ingredient_rel recipe_ingredient_rel_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_ingredient_rel
    ADD CONSTRAINT recipe_ingredient_rel_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id);


--
-- Name: recipe recipe_style_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe
    ADD CONSTRAINT recipe_style_id_fkey FOREIGN KEY (style_id) REFERENCES style(style_id);


--
-- Name: recipe recipe_submitter_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe
    ADD CONSTRAINT recipe_submitter_id_fkey FOREIGN KEY (submitter_id) REFERENCES user_account(user_id);


--
-- Name: recipe_user_rating recipe_user_rating_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_user_rating
    ADD CONSTRAINT recipe_user_rating_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id);


--
-- Name: recipe_user_rating recipe_user_rating_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY recipe_user_rating
    ADD CONSTRAINT recipe_user_rating_user_id_fkey FOREIGN KEY (user_id) REFERENCES user_account(user_id);


--
-- Name: testtable2 testtable2_rid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY testtable2
    ADD CONSTRAINT testtable2_rid_fkey FOREIGN KEY (rid) REFERENCES testtable1(id);


--
-- Name: user_favourite_recipe user_favourite_recipe_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_favourite_recipe
    ADD CONSTRAINT user_favourite_recipe_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES recipe(recipe_id);


--
-- Name: user_favourite_recipe user_favourite_recipe_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_favourite_recipe
    ADD CONSTRAINT user_favourite_recipe_user_id_fkey FOREIGN KEY (user_id) REFERENCES user_account(user_id);


--
-- PostgreSQL database dump complete
--

