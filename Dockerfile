# Custom base image for CircleCI builds. Available at gbalk/baker-street-build:latest

FROM circleci/openjdk:8-jdk

RUN sudo apt-get update \
	&& sudo apt-get install -y sudo python-dev \
	&& cd /tmp \
	&& curl -O https://bootstrap.pypa.io/get-pip.py \
	&& python get-pip.py --user \
	&& $HOME/.local/bin/pip install --user awsebcli --upgrade