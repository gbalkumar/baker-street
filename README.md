# Baker Street

##### [INT Environment](https://int-bakerstreet.global.ssl.fastly.net)
[![CircleCI](https://circleci.com/bb/gbalkumar/baker-street.svg?style=svg)](https://circleci.com/bb/gbalkumar/baker-street)
##### [QA Environment](https://int-bakerstreet.global.ssl.fastly.net)
[![CircleCI](https://circleci.com/bb/gbalkumar/baker-street/tree/master.svg?style=svg)](https://circleci.com/bb/gbalkumar/baker-street/tree/master)

#dependency Environmental variables
1.API_HOSTNAME
2.SENDGRID_API_KEY
3.DB_PASSWORD
4.DB_USERNAME
5.JDBC_CONNECTION_STRING